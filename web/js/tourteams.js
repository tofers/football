$(document).ready(function () {
    $('[name*=team_id]').on('change', function () {
        var v = $(this).val(),
        key=$(this).data('key');
        $.ajax({
            type: "POST",
            url: '/tourteams/player/' + v+'?key='+key,
            success: function (data) {
                $('#player'+key).html(data);
                $('#player'+key).find('.selectpicker').selectpicker();

            },
            error: function (data) {

            }
        });


    });
});

