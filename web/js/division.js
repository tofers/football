$(document).ready(function () {
    $('#division-type').on('change', function () {
        var v = $(this).val();

        $('[id^=division-type-content-]').hide().find('input,select,textarea').prop('disabled', true);
        $('[id^=division-type-content-' + v + ']').show().find('input,select,textarea').prop('disabled', false);
    }).trigger('change');
});

