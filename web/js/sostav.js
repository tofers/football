$(document).ready(function () {
    $('#teamsostav-team_id').on('change', function () {
        var team_id = $(this).val();
        $.ajax({
            type: "POST",
            url: '/sostav/players/' + team_id,
            beforeSend: function () {
                $('#players-list').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
                $('#disq-list').html('<div class="text-center"><i class="fa fa-spinner fa-spin"></i></div>');
            },
            success: function (data) {
                $('#players-list').html(data.players);
                $('#disq-list').html(data.disq);
                $('.switch').bootstrapSwitch();
            },
            error: function (data) {
                toastr['error']('Произошла ошибка, попробуйте позднее.');
            }
        });
    });

    $('.switch').bootstrapSwitch();

    $('.mt-repeater').repeater({
        show: function () {
            $(this).slideDown();
            $('.selectpicker-repeat').selectpicker('refresh');
        },
        hide: function (e) {
            confirm('Удалить элемент?') && $(this).slideUp(e)
        },
        ready: function (e) {
        }
    });

    $('#players-list').on('click', '#checkOn,#checkOff', function () {
        var idt = $(this).attr('id'),
            bi = (idt == 'checkOn'),
            ti = $(this).closest('th').index();
        ti += 1;
        $('#teamsostamplayers tbody tr td:nth-child(' + ti + ')')
            .find('input')
            .prop('checked', bi)
            .trigger('reset.bootstrapSwitch');
        return false;
    });
});

