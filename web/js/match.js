
$(document).ready(function () {

    $('#matchsearch-period').daterangepicker({
            opens: (App.isRTL() ? 'left' : 'right'),
            "showISOWeekNumbers": true,
            "timePicker": true,
            autoUpdateInput: false,
            "timePicker24Hour": true,
            "timePickerSeconds": false,
            "autoApply": true,

            ranges: {
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Последние 7 дней': [moment().subtract('days', 6), moment()],
                'Последние 30 дней': [moment().subtract('days', 29), moment()],
                'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                'Прошлый месяц': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green btn-sm',
            cancelClass: 'default btn-sm',
            separator: ' to ',
            locale: {
                cancelLabel: 'Отменить',
                applyLabel: 'Применить',
                fromLabel: 'От кого',
                toLabel: 'Кому',
                format: "DD.MM.YYYY HH:mm:ss",
                customRangeLabel: 'Задать период',
                daysOfWeek: ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб'],
                monthNames: [
                    'Январь',
                    'Февраль',
                    'Март',
                    'Апрель',
                    'Май',
                    'Июнь',
                    'Июль',
                    'Август',
                    'Сентябрь',
                    'Октябрь',
                    'Ноябрь',
                    'Декабрь'
                ],
                firstDay: 1
            },
            "linkedCalendars": false
        },
        function (start, end, label) {
            $('#matchsearch-period').val(start.format('DD.MM.YYYY HH:mm') + ' - ' + end.format('DD.MM.YYYY HH:mm'));

        }
    );

});
