$(document).ready(function () {
    $('#downloadPhoto').on('click', function () {
        $('#galleryphoto-filename').trigger('click');
    });

    $('#galleryphoto-filename').on('change', function () {

    });
    $('#savePhoto').on('click', function () {
        var formPhotos = $('#formPhotos'),
            form_index = $('form').index(formPhotos),
            formData = new FormData($('form')[form_index]),
            modelId = formPhotos.attr('data-key'),
            bSave = $(this);
        $.ajax({
            type: "POST",
            processData: false,
            contentType: false,
            url: '/gallery/photo/' + modelId,
            data: formData,
            beforeSend: function () {
                bSave.button('loading');
            }
        }).done(function (data) {
            bSave.button('reset');
        }).error(function (jqXHR, textStatus, errorThrown) {
            bSave.button('reset');
            toastr['error'](jqXHR.responseText, 'Ошибка');
        });
    })
});

