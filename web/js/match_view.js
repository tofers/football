
$(document).ready(function () {

    $('.switch').bootstrapSwitch();
    $('.mt-repeater').repeater({
        show: function () {
            $(this).slideDown();
            $('.selectpicker-repeat').selectpicker('refresh');
        },
        hide: function (e) {
            confirm('Удалить элемент?') && $(this).slideUp(e)
        },
        ready: function (e) {
        }
    });

    $('.selectpicker-repeat').selectpicker();
});
