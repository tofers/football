function getChar(event) {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode) // IE
    }

    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which) // РѕСЃС‚Р°Р»СЊРЅС‹Рµ
    }

    return null; // СЃРїРµС†РёР°Р»СЊРЅР°СЏ РєР»Р°РІРёС€Р°
}

$(document).ready(function () {
    $('.summernote').summernote({height: 300});
    $('.selectpicker').selectpicker();
    $('#banner-published, #season-published,#news-published, #division-published, #media-published, #match-was').bootstrapSwitch();

    $('.date').datetimepicker({
        format: 'DD.MM.YYYY'
    });
    $('.datetime').datetimepicker({
        format: 'DD.MM.YYYY HH:mm'
    });

    $('input.number_input').on('keypress', function (e) {
        e = e || event;

        if (e.ctrlKey || e.altKey || e.metaKey) return;

        var chr = getChar(e);
        if (chr == null) return;
        if ((chr < '0' || chr > '9') && chr != ',') {
            return false;
        }
    });

    $('button#saveForm').on('click', function () {
        $('.portlet-body').find('[type=submit]').click();
    });


    $('#header_city select, #header_sport select, #header_season select').on('change', function () {
        $.ajax({
            type: "POST",
            url: '/site/session',
            data: $('#header_city, #header_sport, #header_season').find('select'),
            success: function (data) {
                window.location.reload();
            },
            error: function (data) {

            }
        });
    });

    $('.portlet-body > .grid-view > table > tbody > tr[data-key]').on('click', function (e) {
        if ($(e.target).is('a') || $(e.target).is('i')) {
            return true;
        }
        var h = window.location.href,
            key = $(this).attr('data-key'),
            table = $(this).closest('table');
        if (table.attr('id') == 'table-tourteams') {
            key = $(this).data('key');
            key = key.division_id + '/' + key.tour;
        }
        h = h.split('?')[0];
        h = h.replace('/index', '');
        h = h.replace('index', '');
        window.location.href = h + '/update/' + key;
    })
});

