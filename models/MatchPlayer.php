<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "match_player".
 *
 * @property int $match_id
 * @property int $player_id
 * @property int $team_id
 * @property int $goals
 * @property int $penalties
 * @property int $assistent
 * @property int $cards_yellow
 * @property int $cards_2yellow
 * @property int $cards_red
 * @property int $cards_ball
 */
class MatchPlayer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'match_player';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['match_id', 'player_id', 'team_id', 'goals', 'penalties', 'assistent', 'cards_yellow', 'cards_2yellow', 'cards_red', 'cards_ball'], 'required'],
            [['match_id', 'player_id', 'team_id', 'goals', 'penalties', 'assistent', 'cards_yellow', 'cards_2yellow', 'cards_red', 'cards_ball'], 'integer'],
            [['match_id', 'player_id'], 'unique', 'targetAttribute' => ['match_id', 'player_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'match_id' => 'Match ID',
            'player_id' => 'Player ID',
            'team_id' => 'Team ID',
            'goals' => 'Goals',
            'penalties' => 'Penalties',
            'assistent' => 'Assistent',
            'cards_yellow' => 'Cards Yellow',
            'cards_2yellow' => 'Cards 2yellow',
            'cards_red' => 'Cards Red',
            'cards_ball' => 'Cards Ball',
        ];
    }
}
