<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "division".
 *
 * @property int $id
 * @property int $season_id
 * @property int $published
 * @property int $type
 * @property string $css
 * @property string $title
 * @property string $url
 * @property int $sort
 * @property string $matches
 * @property string $config
 */
class Division extends \yii\db\ActiveRecord
{
    public static $typeName = [
        'Чемпионат',
        'Кубок',
        'Мини-чемпионат',
        'Кубок с выездными полуфиналами'

    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'division';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id', 'published', 'type', 'title', 'url'], 'required'],
            [['season_id'], 'integer'],
            [['css', 'matches', 'config'], 'string'],
            [['published', 'type'], 'string', 'max' => 1],
            [['title', 'url'], 'string', 'max' => 50],
            [['sort'], 'safe'],
            //[['url'],'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'season_id' => 'Season ID',
            'published' => 'Published',
            'type' => 'Тип',
            'type_name' => 'Тип',
            'css' => 'Css',
            'title' => 'Название дивизиона',
            'url' => 'Url',
            'sort' => 'Sort',
            'matches' => 'Matches',
            'config' => 'Config',
        ];
    }

    public function getType_name()
    {
        return self::$typeName[$this->type];
    }
}
