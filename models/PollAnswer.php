<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "poll_answer".
 *
 * @property int $poll_id
 * @property int $answer
 * @property string $ip
 */
class PollAnswer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll_answer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poll_id', 'answer', 'ip'], 'required'],
            [['poll_id', 'answer', 'ip'], 'integer'],
            [['poll_id', 'ip'], 'unique', 'targetAttribute' => ['poll_id', 'ip']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'poll_id' => 'Poll ID',
            'answer' => 'Answer',
            'ip' => 'Ip',
        ];
    }
}
