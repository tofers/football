<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "poll".
 *
 * @property int $id
 * @property string $question
 * @property string $answers
 */
class Poll extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'answers'], 'required'],
            [['answers'], 'string'],
            [['question'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answers' => 'Ответ',
        ];
    }
}
