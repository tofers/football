<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Match;

/**
 * MatchSearch represents the model behind the search form of `app\models\Match`.
 */
class MatchSearch extends Match
{
    public $period;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'division_id', 'stadium', 'area', 'tour', 'referee_id', 'referee2_id', 'team1_id', 'team1_score', 'team1_penalties', 'team1_goals1', 'team1_goals2', 'team1_shots1', 'team1_shots2', 'team1_gate1', 'team1_gate2', 'team1_corner1', 'team1_corner2', 'team1_rail1', 'team1_rail2', 'team1_fouls1', 'team1_fouls2', 'team2_id', 'team2_score', 'team2_penalties', 'team2_goals1', 'team2_goals2', 'team2_shots1', 'team2_shots2', 'team2_gate1', 'team2_gate2', 'team2_corner1', 'team2_corner2', 'team2_rail1', 'team2_rail2', 'team2_fouls1', 'team2_fouls2', 'sort', 'video_id'], 'integer'],
            [['period', 'date', 'tech', 'was', 'text', 'team1_name', 'team1_penalties_array', 'team1_goals_array', 'team1_cards_array', 'team1_referee', 'team1_discipline', 'team2_name', 'team2_penalties_array', 'team2_goals_array', 'team2_cards_array', 'team2_referee', 'team2_discipline'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $divisions = Division::find()->select(['id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();
        $query = Match::find()->with(['team1', 'team2', 'division'])->where([
            'division_id' => $divisions
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'division_id' => $this->division_id,
            'date' => $this->date,
            'stadium' => $this->stadium,
            'area' => $this->area,
            'tour' => $this->tour,
            'referee_id' => $this->referee_id,
            'referee2_id' => $this->referee2_id,
            'team1_id' => $this->team1_id,
            'team1_score' => $this->team1_score,
            'team1_penalties' => $this->team1_penalties,
            'team1_goals1' => $this->team1_goals1,
            'team1_goals2' => $this->team1_goals2,
            'team1_shots1' => $this->team1_shots1,
            'team1_shots2' => $this->team1_shots2,
            'team1_gate1' => $this->team1_gate1,
            'team1_gate2' => $this->team1_gate2,
            'team1_corner1' => $this->team1_corner1,
            'team1_corner2' => $this->team1_corner2,
            'team1_rail1' => $this->team1_rail1,
            'team1_rail2' => $this->team1_rail2,
            'team1_fouls1' => $this->team1_fouls1,
            'team1_fouls2' => $this->team1_fouls2,
            'team2_id' => $this->team2_id,
            'team2_score' => $this->team2_score,
            'team2_penalties' => $this->team2_penalties,
            'team2_goals1' => $this->team2_goals1,
            'team2_goals2' => $this->team2_goals2,
            'team2_shots1' => $this->team2_shots1,
            'team2_shots2' => $this->team2_shots2,
            'team2_gate1' => $this->team2_gate1,
            'team2_gate2' => $this->team2_gate2,
            'team2_corner1' => $this->team2_corner1,
            'team2_corner2' => $this->team2_corner2,
            'team2_rail1' => $this->team2_rail1,
            'team2_rail2' => $this->team2_rail2,
            'team2_fouls1' => $this->team2_fouls1,
            'team2_fouls2' => $this->team2_fouls2,
            'sort' => $this->sort,
            'video_id' => $this->video_id,
        ]);

        $query->andFilterWhere(['like', 'tech', $this->tech])
            ->andFilterWhere(['like', 'was', $this->was])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'team1_name', $this->team1_name])
            ->andFilterWhere(['like', 'team1_penalties_array', $this->team1_penalties_array])
            ->andFilterWhere(['like', 'team1_goals_array', $this->team1_goals_array])
            ->andFilterWhere(['like', 'team1_cards_array', $this->team1_cards_array])
            ->andFilterWhere(['like', 'team1_referee', $this->team1_referee])
            ->andFilterWhere(['like', 'team1_discipline', $this->team1_discipline])
            ->andFilterWhere(['like', 'team2_name', $this->team2_name])
            ->andFilterWhere(['like', 'team2_penalties_array', $this->team2_penalties_array])
            ->andFilterWhere(['like', 'team2_goals_array', $this->team2_goals_array])
            ->andFilterWhere(['like', 'team2_cards_array', $this->team2_cards_array])
            ->andFilterWhere(['like', 'team2_referee', $this->team2_referee])
            ->andFilterWhere(['like', 'team2_discipline', $this->team2_discipline]);

        if ($this->period) {
            $period = explode(' - ', $this->period);
            $start = strtotime($period[0]);
            $start = date('Y-m-d H:i', $start);
            $end = strtotime($period[1]);
            $end = date('Y-m-d H:i', $end);
            $query->andFilterWhere([
                'between',
                'date',
                $start,
                $end
            ]);
        }

        return $dataProvider;
    }
}
