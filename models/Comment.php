<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $type
 * @property int $link_id
 * @property string $time
 * @property int $user_id
 * @property string $text
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'link_id', 'user_id', 'text'], 'required'],
            [['type', 'text'], 'string'],
            [['link_id', 'user_id'], 'integer'],
            [['time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'link_id' => 'Link ID',
            'time' => 'Time',
            'user_id' => 'User ID',
            'text' => 'Text',
        ];
    }
}
