<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "team_sostav".
 *
 * @property int $id
 * @property int $season_id
 * @property int $team_id
 * @property string $players
 * @property string $player
 * @property string $revoked
 * @property string $administration
 * @property string $disc
 *
 * @property Team $team
 */
class TeamSostav extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team_sostav';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id', 'team_id'], 'required'],
            [['season_id', 'team_id'], 'integer'],
            //[['season_id', 'team_id'], 'unique'],
            [['players', 'revoked', 'administration', 'disc'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'season_id' => 'Season ID',
            'team_id' => 'Команда',
            'team.name' => 'Команда',
            'players' => 'Игроки',
            'player' => 'Игроки',
            'revoked' => 'Revoked',
            'administration' => 'Администрация',
            'disc' => 'Disc',
        ];
    }

    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    public function getPlayer()
    {
        $players = explode(',', $this->players);
        $players = Player::findAll($players);
        $players = ArrayHelper::map($players, 'id', 'full_name');
        return implode(', ', $players);
    }
}
