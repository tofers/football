<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "gallery_photo".
 *
 * @property int $id
 * @property int $gallery_id
 * @property string $filename
 */
class GalleryPhoto extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_photo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gallery_id', 'filename'], 'required'],
            [['gallery_id'], 'integer'],
            [['filename'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gallery_id' => 'Gallery ID',
            'filename' => 'Filename',
        ];
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'gallery';
        $id = Yii::$app->request->get('id');

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }
        $path = $city . '/' . $sport . '/' . $name_model . '/' . $id;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }

        $photos = UploadedFile::getInstances($this, 'filename');
        if ($photos) {
            foreach ($photos as $photo) {
                $tmp = $photo->tempName;
                $size = $photo->size;
                $fnTemp = explode(".", $photo->name);
                $fnTemp = end($fnTemp);
                $ext = strtolower($fnTemp); // расширение файла
                $name = md5($photo->name . $path) . '.' . $ext;
                //$name = str_ireplace('.' . $ext, '', $photo->name);
                $file_path = $path . '/' . $name;
                $photo->saveAs($file_path);

                $mPhoto = new self();
                $mPhoto->filename = '/' . $file_path;
                $mPhoto->gallery_id = $id;
                $mPhoto->save();
            }
        }

        return true;
    }
}
