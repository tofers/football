<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "season".
 *
 * @property int $id
 * @property int $city_id
 * @property string $url
 * @property string $title
 * @property string $start_date
 * @property int $published
 */
class Season extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'season';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'title', 'start_date', 'published'], 'required'],
            //[['city_id'], 'integer'],
            [['start_date'], 'safe'],
            [['url', 'title'], 'string', 'max' => 100],
            [['published'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'url' => 'Url',
            'title' => 'Название',
            'start_date' => 'Дата начала',
            'published' => 'Published',
        ];
    }

    public function getYear()
    {
        return preg_replace("/[^0-9]/", '', $this->title);
    }
}
