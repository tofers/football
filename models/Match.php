<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "match".
 *
 * @property int $id
 * @property int $division_id
 * @property string $date
 * @property int $stadium
 * @property int $area
 * @property int $tour
 * @property int $referee_id
 * @property int $referee2_id
 * @property int $tech
 * @property int $was
 * @property string $text
 * @property int $team1_id
 * @property string $team1_name
 * @property int $team1_score
 * @property int $team1_penalties
 * @property string $team1_penalties_array
 * @property string $team1_goals_array
 * @property int $team1_goals1
 * @property int $team1_goals2
 * @property int $team1_shots1
 * @property int $team1_shots2
 * @property int $team1_gate1
 * @property int $team1_gate2
 * @property int $team1_corner1
 * @property int $team1_corner2
 * @property int $team1_rail1
 * @property int $team1_rail2
 * @property int $team1_fouls1
 * @property int $team1_fouls2
 * @property string $team1_cards_array
 * @property int $team1_referee
 * @property int $team1_discipline
 * @property int $team2_id
 * @property string $team2_name
 * @property int $team2_score
 * @property int $team2_penalties
 * @property string $team2_penalties_array
 * @property string $team2_goals_array
 * @property int $team2_goals1
 * @property int $team2_goals2
 * @property int $team2_shots1
 * @property int $team2_shots2
 * @property int $team2_gate1
 * @property int $team2_gate2
 * @property int $team2_corner1
 * @property int $team2_corner2
 * @property int $team2_rail1
 * @property int $team2_rail2
 * @property int $team2_fouls1
 * @property int $team2_fouls2
 * @property string $team2_cards_array
 * @property int $team2_referee
 * @property int $team2_discipline
 * @property int $sort
 * @property int $video_id
 * @property string $name
 * @property string $nameandtour
 * @property string $nameandtouranddiv
 * @property string $tourname
 *
 * @property Division $division
 * @property Team $team1
 * @property Team $team2
 * @property Referee $referee1
 * @property Referee $referee2
 * @property MatchPlayer[] $players1
 * @property MatchPlayer[] $players2
 */
class Match extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'match';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['division_id', 'area', 'tour', 'tech', 'was', 'text', 'team1_id', 'team1_name', 'team1_score', 'team1_penalties', 'team1_penalties_array', 'team1_goals_array', 'team1_goals1', 'team1_goals2', 'team1_shots1', 'team1_shots2', 'team1_gate1', 'team1_gate2', 'team1_corner1', 'team1_corner2', 'team1_rail1', 'team1_rail2', 'team1_fouls1', 'team1_fouls2', 'team1_cards_array', 'team1_referee', 'team1_discipline', 'team2_id', 'team2_name', 'team2_score', 'team2_penalties', 'team2_penalties_array', 'team2_goals_array', 'team2_goals1', 'team2_goals2', 'team2_shots1', 'team2_shots2', 'team2_gate1', 'team2_gate2', 'team2_corner1', 'team2_corner2', 'team2_rail1', 'team2_rail2', 'team2_fouls1', 'team2_fouls2', 'team2_cards_array', 'team2_referee', 'team2_discipline', 'stadium'], 'required'],
            [['division_id', 'stadium', 'area', 'tour', 'referee_id', 'referee2_id', 'team1_id', 'team1_score', 'team1_penalties', 'team1_goals1', 'team1_goals2', 'team1_shots1', 'team1_shots2', 'team1_gate1', 'team1_gate2', 'team1_corner1', 'team1_corner2', 'team1_rail1', 'team1_rail2', 'team1_fouls1', 'team1_fouls2', 'team2_id', 'team2_score', 'team2_penalties', 'team2_goals1', 'team2_goals2', 'team2_shots1', 'team2_shots2', 'team2_gate1', 'team2_gate2', 'team2_corner1', 'team2_corner2', 'team2_rail1', 'team2_rail2', 'team2_fouls1', 'team2_fouls2', 'sort', 'video_id'], 'integer'],
            [['date'], 'safe'],
            [['text', 'team1_penalties_array', 'team1_goals_array', 'team1_cards_array', 'team2_penalties_array', 'team2_goals_array', 'team2_cards_array'], 'string'],
            [['tech', 'was', 'team1_referee', 'team1_discipline', 'team2_referee', 'team2_discipline'], 'string', 'max' => 1],
            [['team1_name', 'team2_name'], 'string', 'max' => 50],
            [['referee_id', 'referee2_id'], 'default', 'value' => 0],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'division_id' => 'Дивизион',
            'date' => 'Дата и время',
            'period' => 'Дата и время',
            'stadium' => 'Стадион',
            'area' => 'Поле',
            'areaname' => 'Поле',
            'tour' => 'Тур',
            'tourname' => 'Тур',
            'referee_id' => 'Судья',
            'referee2_id' => 'Судья 2',
            'tech' => 'Техническое поражение:',
            'was' => 'Матч прошел',
            'text' => 'Text',
            'team1_id' => 'Хозяева',
            'team1_name' => 'Team1 Name',
            'team1_score' => 'Team1 Score',
            'team1_penalties' => 'Team1 Penalties',
            'team1_penalties_array' => 'Team1 Penalties Array',
            'team1_goals_array' => 'Team1 Goals Array',
            'team1_goals1' => 'Team1 Goals1',
            'team1_goals2' => 'Team1 Goals2',
            'team1_shots1' => 'Team1 Shots1',
            'team1_shots2' => 'Team1 Shots2',
            'team1_gate1' => 'Team1 Gate1',
            'team1_gate2' => 'Team1 Gate2',
            'team1_corner1' => 'Team1 Corner1',
            'team1_corner2' => 'Team1 Corner2',
            'team1_rail1' => 'Team1 Rail1',
            'team1_rail2' => 'Team1 Rail2',
            'team1_fouls1' => 'Team1 Fouls1',
            'team1_fouls2' => 'Team1 Fouls2',
            'team1_cards_array' => 'Team1 Cards Array',
            'team1_referee' => 'Team1 Referee',
            'team1_discipline' => 'Team1 Discipline',
            'team2_id' => 'Гости',
            'team2_name' => 'Team2 Name',
            'team2_score' => 'Team2 Score',
            'team2_penalties' => 'Team2 Penalties',
            'team2_penalties_array' => 'Team2 Penalties Array',
            'team2_goals_array' => 'Team2 Goals Array',
            'team2_goals1' => 'Team2 Goals1',
            'team2_goals2' => 'Team2 Goals2',
            'team2_shots1' => 'Team2 Shots1',
            'team2_shots2' => 'Team2 Shots2',
            'team2_gate1' => 'Team2 Gate1',
            'team2_gate2' => 'Team2 Gate2',
            'team2_corner1' => 'Team2 Corner1',
            'team2_corner2' => 'Team2 Corner2',
            'team2_rail1' => 'Team2 Rail1',
            'team2_rail2' => 'Team2 Rail2',
            'team2_fouls1' => 'Team2 Fouls1',
            'team2_fouls2' => 'Team2 Fouls2',
            'team2_cards_array' => 'Team2 Cards Array',
            'team2_referee' => 'Team2 Referee',
            'team2_discipline' => 'Team2 Discipline',
            'sort' => 'Sort',
            'video_id' => 'Видео',
            'team1.name' => 'Хозяева',
            'team2.name' => 'Гости',
            'score' => 'Счет',
        ];
    }

    public function getPlayers1()
    {
        return MatchPlayer::find()->where(['team_id' => $this->team1_id, 'match_id' => $this->id])->all();
    }

    public function getPlayers2()
    {
        return $this->hasMany(MatchPlayer::className(), ['match_id' => 'id'])->where(['team_id' => $this->team2_id]);
    }

    public function getDivision()
    {
        return $this->hasOne(Division::className(), ['id' => 'division_id']);
    }

    public function getReferee1()
    {
        return $this->hasOne(Referee::className(), ['id' => 'referee_id']);
    }

    public function getReferee2()
    {
        return $this->hasOne(Referee::className(), ['id' => 'referee2_id']);
    }

    public function getTeam1()
    {
        return $this->hasOne(Team::className(), ['id' => 'team1_id']);
    }

    public function getTeam2()
    {
        return $this->hasOne(Team::className(), ['id' => 'team2_id']);
    }

    public function getAreaName()
    {
        return 'Поле №' . $this->area;
    }

    public function getTourName()
    {
        return 'Тур ' . $this->tour;
    }

    public function getName()
    {
        return $this->team1_name . ' - ' . $this->team2_name;
    }

    public function getNameAndTour()
    {
        return $this->tourname . '. ' . $this->team1_name . ' - ' . $this->team2_name;
    }

    public function getNameAndTourAndDiv()
    {
        return $this->division->title . ' ' . $this->tourname . '. ' . $this->team1_name . ' - ' . $this->team2_name;
    }

    public function getScore()
    {
        $tech = $this->tech ? ' Т' : '';
        return $this->team1_score . ':' . $this->team2_score . $tech;
    }
}
