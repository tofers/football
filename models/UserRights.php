<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_rights".
 *
 * @property string $module
 * @property string $right
 * @property int $user_id
 */
class UserRights extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_rights';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'right', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['module', 'right'], 'string', 'max' => 50],
            [['module', 'right', 'user_id'], 'unique', 'targetAttribute' => ['module', 'right', 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'module' => 'Module',
            'right' => 'Right',
            'user_id' => 'User ID',
        ];
    }
}
