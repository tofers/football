<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "banner".
 *
 * @property int $id
 * @property string $title
 * @property string $link
 * @property string $filename
 * @property string $html_code
 * @property int $width
 * @property int $height
 * @property int $published
 * @property int $type
 * @property int $shows
 */
class Banner extends \yii\db\ActiveRecord
{
    public static $typeName =[
        'Верхний',
        'Боковой',
        'На главной Шапка-События',
        'На главной События-Матчи',
        'На главной Матч-Опросы',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'link', 'filename', 'published', 'type', 'shows'], 'required'],
            [['html_code'], 'string'],
            [['width', 'height', 'shows'], 'integer'],
            [['title', 'link', 'filename'], 'string', 'max' => 255],
            [['published', 'type'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'link' => 'Ссылка',
            'filename' => 'Файл баннера',
            'html_code' => 'Html Code',
            'width' => 'Ширина',
            'height' => 'Высота',
            'published' => 'Публикация',
            'status' => 'Активность',
            'type' => 'Тип',
            'type_name' => 'Тип',
            'shows' => 'Показов',
        ];
    }

    public function getType_name()
    {
        return self::$typeName[$this->type];
    }

    public function getStatus()
    {
        $color = $this->published?'font-green':'font-red';
        return '<i class="icon-power '.$color.'"></i>';
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'banner';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }


        $photo = UploadedFile::getInstance($this, 'filename');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name.$path).'.'.$ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->filename = '/'.$file_path;
        }
        return true;
    }
}
