<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TeamSostav;

/**
 * SostavSearch represents the model behind the search form of `app\models\TeamSostav`.
 */
class SostavSearch extends TeamSostav
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'season_id', 'team_id'], 'integer'],
            [['players', 'revoked', 'administration', 'disc'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TeamSostav::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $this->season_id = Yii::$app->session->get('season');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'season_id' => $this->season_id,
            'team_id' => $this->team_id,
        ]);

        $query->andFilterWhere(['like', 'players', $this->players])
            ->andFilterWhere(['like', 'revoked', $this->revoked])
            ->andFilterWhere(['like', 'administration', $this->administration])
            ->andFilterWhere(['like', 'disc', $this->disc]);

        return $dataProvider;
    }
}
