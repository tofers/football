<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "season_team".
 *
 * @property int $season_id
 * @property int $division_id
 * @property int $team_id
 * @property int $games
 * @property int $games_win
 * @property int $games_draw
 * @property int $games_lose
 * @property int $games_win_pos
 * @property int $games_lose_pos
 * @property int $goals
 * @property int $goals_conceded
 * @property int $games_zero
 * @property int $cards_yellow
 * @property int $cards_2yellow
 * @property int $cards_red
 * @property int $cards_ball
 * @property double $referee
 * @property double $discipline
 * @property int $shots
 * @property int $shots_sop
 * @property int $gate
 * @property int $gate_sop
 * @property int $corner
 * @property int $corner_sop
 * @property int $rail
 * @property int $rail_sop
 * @property int $fouls
 * @property int $fouls_sop
 * @property string $goals_by_time
 */
class SeasonTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'season_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id', 'division_id', 'team_id', 'games', 'games_win', 'games_draw', 'games_lose', 'goals', 'goals_conceded', 'games_zero', 'cards_yellow', 'cards_2yellow', 'cards_red', 'cards_ball', 'referee', 'discipline', 'shots', 'shots_sop', 'gate', 'gate_sop', 'corner', 'corner_sop', 'rail', 'rail_sop', 'fouls', 'fouls_sop', 'goals_by_time'], 'required'],
            [['season_id', 'division_id', 'team_id', 'games', 'games_win', 'games_draw', 'games_lose', 'games_win_pos', 'games_lose_pos', 'goals', 'goals_conceded', 'games_zero', 'cards_yellow', 'cards_2yellow', 'cards_red', 'cards_ball', 'shots', 'shots_sop', 'gate', 'gate_sop', 'corner', 'corner_sop', 'rail', 'rail_sop', 'fouls', 'fouls_sop'], 'integer'],
            [['referee', 'discipline'], 'number'],
            [['goals_by_time'], 'string'],
            [['season_id', 'team_id'], 'unique', 'targetAttribute' => ['season_id', 'team_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'season_id' => 'Season ID',
            'division_id' => 'Division ID',
            'team_id' => 'Team ID',
            'games' => 'Games',
            'games_win' => 'Games Win',
            'games_draw' => 'Games Draw',
            'games_lose' => 'Games Lose',
            'games_win_pos' => 'Games Win Pos',
            'games_lose_pos' => 'Games Lose Pos',
            'goals' => 'Goals',
            'goals_conceded' => 'Goals Conceded',
            'games_zero' => 'Games Zero',
            'cards_yellow' => 'Cards Yellow',
            'cards_2yellow' => 'Cards 2yellow',
            'cards_red' => 'Cards Red',
            'cards_ball' => 'Cards Ball',
            'referee' => 'Referee',
            'discipline' => 'Discipline',
            'shots' => 'Shots',
            'shots_sop' => 'Shots Sop',
            'gate' => 'Gate',
            'gate_sop' => 'Gate Sop',
            'corner' => 'Corner',
            'corner_sop' => 'Corner Sop',
            'rail' => 'Rail',
            'rail_sop' => 'Rail Sop',
            'fouls' => 'Fouls',
            'fouls_sop' => 'Fouls Sop',
            'goals_by_time' => 'Goals By Time',
        ];
    }
}
