<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gallery_photo_link".
 *
 * @property int $photo_id
 * @property string $type
 * @property int $link_id
 */
class GalleryPhotoLink extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery_photo_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo_id', 'type', 'link_id'], 'required'],
            [['photo_id', 'link_id'], 'integer'],
            [['type'], 'string'],
            [['photo_id', 'type', 'link_id'], 'unique', 'targetAttribute' => ['photo_id', 'type', 'link_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'photo_id' => 'Photo ID',
            'type' => 'Type',
            'link_id' => 'Link ID',
        ];
    }
}
