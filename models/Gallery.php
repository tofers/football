<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "gallery".
 *
 * @property int $id
 * @property int $season_id
 * @property string $title
 * @property string $date
 * @property string $logo
 * @property string $archive_url
 *
 * @property GalleryPhoto[] $photos
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gallery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id', 'title', 'logo'], 'required'],
            [['season_id'], 'integer'],
            [['date'], 'safe'],
            [['title', 'logo', 'archive_url'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'season_id' => 'Season ID',
            'title' => 'Название',
            'date' => 'Дата',
            'logo' => 'Логотип',
            'archive_url' => 'Ссылка на архив',
            'photoscount' => 'Ко-во фотографий',
        ];
    }

    public function getPhotos()
    {
        return $this->hasMany(GalleryPhoto::className(), ['gallery_id' => 'id']);
    }

    public function getPhotosCount()
    {
        if($this->photos){
            return count($this->photos);
        }
        return 0;
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'gallery';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }


        $photo = UploadedFile::getInstance($this, 'logo');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name . $path) . '.' . $ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->logo = '/' . $file_path;
        }

        return true;
    }

}
