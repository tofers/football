<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "runstring".
 *
 * @property int $id
 * @property string $text
 * @property string $start_date
 * @property string $end_date
 */
class Runstring extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'runstring';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'start_date', 'end_date'], 'required'],
            [['text'], 'string'],
            [['start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Сообщение',
            'start_date' => 'Выводить с',
            'end_date' => 'Вводить до',
        ];
    }
}
