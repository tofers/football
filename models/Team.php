<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $company
 * @property string $description
 * @property string $text
 * @property string $photo
 * @property string $site
 * @property string $facebook
 * @property string $twitter
 * @property string $vkontakte
 * @property string $trainer
 * @property string $mini_logo
 *
 * @property Player[] $players
 * @property Player[] $playerssostav
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name',  'company', 'description', 'text', 'site', 'facebook', 'twitter', 'vkontakte', 'trainer' ], 'required'],
            [['description', 'text'], 'string'],
            [['name'], 'string', 'max' => 50],
            [['logo', 'company', 'photo', 'site', 'trainer', 'mini_logo'], 'string', 'max' => 100],
            [['facebook', 'twitter', 'vkontakte'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'logo' => 'Логотип',
            'company' => 'Компания',
            'description' => 'Краткое описание',
            'text' => 'Описание',
            'photo' => 'Фото команды',
            'site' => 'Сайт',
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'vkontakte' => 'Vkontakte',
            'trainer' => 'Тренер',
            'mini_logo' => 'Иконка',
            'playerCount' => 'Количество игроков',
        ];
    }

    public function getPlayersSostav()
    {
        $teams_sostav = TeamSostav::find()->where([
            'season_id' => Yii::$app->session->get('season'),
            'team_id' => $this->id
        ])->one();
        $teams_sostav = explode(',', $teams_sostav->players);
        return Player::find()->where(['id' => $teams_sostav])->orderBy(['full_name' => SORT_ASC])->all();
    }

    public function getPlayers()
    {
        return $this->hasMany(Player::className(), ['team_id' => 'id']);
    }

    public function getPlayerCount()
    {
        if ($this->players) {
            return count($this->players);
        }
        return 0;
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'team';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }

        $photo = UploadedFile::getInstance($this, 'photo');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name . $path) . '.' . $ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->photo = '/' . $file_path;
        }
        $photo = UploadedFile::getInstance($this, 'logo');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name . $path) . '.' . $ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->logo = '/' . $file_path;
        }
        $photo = UploadedFile::getInstance($this, 'mini_logo');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name . $path) . '.' . $ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->mini_logo = '/' . $file_path;
        }
        return true;
    }
}
