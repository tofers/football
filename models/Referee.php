<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "referee".
 *
 * @property int $id
 * @property string $full_name
 * @property string $name
 * @property string $photo
 */
class Referee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'referee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'name'], 'required'],
            [['full_name', 'name'], 'string', 'max' => 100],
            [['photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'ФИО',
            'name' => 'Сокращенная запись имени',
            'photo' => 'Фото судьи',
        ];
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'referee';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }

        $photo = UploadedFile::getInstance($this, 'photo');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name.$path).'.'.$ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->photo = '/'.$file_path;
        }
        return true;
    }
}
