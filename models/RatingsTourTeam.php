<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ratings_tour_team".
 *
 * @property int $tour
 * @property int $division_id
 * @property int $position
 * @property int $season_id
 * @property int $team_id
 * @property int $player_id
 * @property string $description
 *
 * @property Team $team
 * @property Player $player
 */
class RatingsTourTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ratings_tour_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour', 'division_id', 'position', 'season_id', 'team_id', 'player_id'], 'required'],
            [['tour', 'division_id', 'season_id', 'team_id', 'player_id'], 'integer'],
            [['description'], 'string'],
            [['position'], 'string', 'max' => 1],
            [['tour', 'division_id', 'position'], 'unique', 'targetAttribute' => ['tour', 'division_id', 'position']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tour' => 'Тур',
            'division_id' => 'Дивизион',
            'position' => 'Позиция',
            'season_id' => 'Season ID',
            'team_id' => 'Команда',
            'player_id' => 'Игрок',
            'description' => 'Описание',
            'division.title' => 'Дивизион',
            'season.title' => 'Сезон',
            'team.name' => 'Команда',
            'players' => 'Игроки',
            'player.full_name' => 'Игрок',
        ];
    }

    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    public function getDivision()
    {
        return $this->hasOne(Division::className(), ['id' => 'division_id']);
    }

    public function getSeason()
    {
        return $this->hasOne(Season::className(), ['id' => 'season_id']);
    }

    public function getPlayer()
    {
        return $this->hasOne(Player::className(), ['id' => 'player_id']);
    }

    public function getPlayerName()
    {
        return $this->player->full_name . ' (' . $this->team->name . ')';
    }

    public function getPlayers()
    {
        $sostav = self::findAll([
            'division_id' => $this->division_id,
            'tour' => $this->tour
        ]);

        $sostav = ArrayHelper::map($sostav, 'player_id', 'playername');

        return implode('<br>', $sostav);
    }
}
