<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "division_team".
 *
 * @property int $division_id
 * @property int $team_id
 * @property int $games
 * @property int $games_win
 * @property int $games_lose
 * @property int $games_draw
 * @property int $games_win_pos
 * @property int $games_lose_pos
 * @property int $goals
 * @property int $goals_conceded
 * @property int $score
 * @property int $place
 */
class DivisionTeam extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'division_team';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['division_id', 'team_id', 'games', 'games_win', 'games_lose', 'games_draw', 'goals', 'goals_conceded', 'score', 'place'], 'required'],
            [['division_id', 'team_id', 'games', 'games_win', 'games_lose', 'games_draw', 'games_win_pos', 'games_lose_pos', 'goals', 'goals_conceded', 'score', 'place'], 'integer'],
            [['division_id', 'team_id'], 'unique', 'targetAttribute' => ['division_id', 'team_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'division_id' => 'Division ID',
            'team_id' => 'Team ID',
            'games' => 'Games',
            'games_win' => 'Games Win',
            'games_lose' => 'Games Lose',
            'games_draw' => 'Games Draw',
            'games_win_pos' => 'Games Win Pos',
            'games_lose_pos' => 'Games Lose Pos',
            'goals' => 'Goals',
            'goals_conceded' => 'Goals Conceded',
            'score' => 'Score',
            'place' => 'Place',
        ];
    }
}
