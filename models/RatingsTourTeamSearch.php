<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RatingsTourTeam;

/**
 * RatingsTourTeamSearch represents the model behind the search form of `app\models\RatingsTourTeam`.
 */
class RatingsTourTeamSearch extends RatingsTourTeam
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour', 'division_id', 'season_id', 'team_id', 'player_id'], 'integer'],
            [['position', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RatingsTourTeam::find()->with(['division', 'player', 'team']);

        $query->groupBy(['division_id', 'tour']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $this->season_id = Yii::$app->session->get('season');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tour' => $this->tour,
            'division_id' => $this->division_id,
            'season_id' => $this->season_id,
            'team_id' => $this->team_id,
            'player_id' => $this->player_id,
        ]);

        $query->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
