<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "media".
 *
 * @property int $id
 * @property int $published
 * @property string $date
 * @property string $link
 * @property string $title
 * @property string $text
 * @property string $img
 */
class Media extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['link', 'title', 'text', 'img'], 'required'],
            [['published'], 'string', 'max' => 1],
            [['link', 'title', 'text', 'img'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'published' => 'Публикация',
            'date' => 'Дата',
            'link' => 'Ссылка',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'img' => 'Логотип',
        ];
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'news';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }


        $photo = UploadedFile::getInstance($this, 'img');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name.$path).'.'.$ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->img = '/'.$file_path;
        }
        return true;
    }
}
