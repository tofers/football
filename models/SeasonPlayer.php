<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "season_player".
 *
 * @property int $season_id
 * @property int $division_id
 * @property int $player_id
 * @property int $team_id
 * @property int $games
 * @property int $goals
 * @property int $penalties
 * @property int $assistent
 * @property int $cards_yellow
 * @property int $cards_2yellow
 * @property int $cards_red
 * @property int $cards_ball
 */
class SeasonPlayer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'season_player';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['season_id', 'division_id', 'player_id', 'team_id', 'games', 'goals', 'penalties', 'assistent', 'cards_yellow', 'cards_2yellow', 'cards_red', 'cards_ball'], 'required'],
            [['season_id', 'division_id', 'player_id', 'team_id', 'games', 'goals', 'penalties', 'assistent', 'cards_yellow', 'cards_2yellow', 'cards_red', 'cards_ball'], 'integer'],
            [['season_id', 'player_id'], 'unique', 'targetAttribute' => ['season_id', 'player_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'season_id' => 'Season ID',
            'division_id' => 'Division ID',
            'player_id' => 'Player ID',
            'team_id' => 'Team ID',
            'games' => 'Games',
            'goals' => 'Goals',
            'penalties' => 'Penalties',
            'assistent' => 'Assistent',
            'cards_yellow' => 'Cards Yellow',
            'cards_2yellow' => 'Cards 2yellow',
            'cards_red' => 'Cards Red',
            'cards_ball' => 'Cards Ball',
        ];
    }
}
