<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "partner".
 *
 * @property int $id
 * @property string $title
 * @property string $link
 * @property string $filename
 * @property int $published
 * @property int $sort
 */
class Partner extends \yii\db\ActiveRecord
{
    public static $typeName =[
        'Скрыт ',
        'Верх',
        'Низ 1-я строка',
        'Низ 2-я строка'
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'link', 'published', 'sort'], 'required'],
            [['sort'], 'integer'],
            [['title', 'link', 'filename'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'link' => 'Ссылка',
            'filename' => 'Логотип',
            'published' => 'Публикация',
            'type_name' => 'Публикация',
            'sort' => 'Sort',
        ];
    }

    public function getType_name()
    {
        return self::$typeName[$this->published];
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'partner';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }

        $photo = UploadedFile::getInstance($this, 'filename');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name.$path).'.'.$ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->filename = '/'.$file_path;
        }

        return true;
    }
}
