<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property int $id
 * @property string $url
 * @property string $locale
 * @property int $published
 * @property string $caption
 * @property string $title
 * @property string $text
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $template
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'locale', 'published', 'caption', 'title', 'text', 'meta_keywords', 'meta_description', 'template'], 'required'],
            [['text'], 'string'],
            [['url', 'title', 'meta_keywords', 'meta_description'], 'string', 'max' => 255],
            [['locale'], 'string', 'max' => 5],
            [['published'], 'string', 'max' => 1],
            [['caption'], 'string', 'max' => 100],
            [['template'], 'string', 'max' => 50],
            [['url'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'locale' => 'Locale',
            'published' => 'Published',
            'caption' => 'Caption',
            'title' => 'Title',
            'text' => 'Text',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'template' => 'Template',
        ];
    }
}
