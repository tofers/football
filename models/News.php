<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $date
 * @property string $title
 * @property string $img
 * @property int $published
 * @property string $description
 * @property string $text
 * @property string $matches
 * @property string $link
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'title', 'img', 'published', 'description', 'text'], 'required'],
            [['date'], 'safe'],
            [['description', 'text'], 'string'],
            [['matches'], 'safe'],
            [['title', 'img', 'link'], 'string', 'max' => 255],
            [['published'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'title' => 'Заголовок',
            'img' => 'Картинка',
            'published' => 'Публикация',
            'status' => 'Публикация',
            'description' => 'Краткое описание',
            'text' => 'Текст',
            'matches' => 'Связь с матчем',
            'link' => 'Ссылка',
        ];
    }

    public function getStatus()
    {
        $color = $this->published ? 'font-green' : 'font-red';
        return '<i class="icon-power ' . $color . '"></i>';
    }

    public function upload()
    {

        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'news';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }


        $photo = UploadedFile::getInstance($this, 'img');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name . $path) . '.' . $ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $photo->saveAs($file_path);
            $this->img = '/' . $file_path;
        }
        return true;
    }
}
