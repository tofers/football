<?php

namespace app\models;

use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "player".
 *
 * @property int $id
 * @property string $full_name
 * @property string $name
 * @property string $birthdate
 * @property int $weight
 * @property int $growth
 * @property string $work_place
 * @property string $photo
 * @property int $team_id
 * @property string $position
 */
class Player extends \yii\db\ActiveRecord
{
    public static $namePosition = [
        'Полузащитник' => 'Полузащитник',
        'Полевой игрок' => 'Полевой игрок',
        'Нападающий' => 'Нападающий',
        'Защитник' => 'Защитник',
        'Вратарь' => 'Вратарь',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'player';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'name', 'birthdate', 'weight', 'growth', 'work_place',  'team_id', 'position'], 'required'],
            [['birthdate'], 'safe'],
            [['weight', 'growth', 'team_id'], 'integer'],
            [['full_name', 'name', 'work_place', 'photo'], 'string', 'max' => 100],
            [['position'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'ФИО',
            'name' => 'Сокращенная запись имени',
            'birthdate' => 'Дата рождения',
            'bthdate' => 'Дата рождения',
            'weight' => 'Вес',
            'growth' => 'Рост',
            'work_place' => 'Должность',
            'photo' => 'Фото игрока',
            'team_id' => 'Команда',
            'team.name' => 'Команда',
            'position' => 'Амплуа',
        ];
    }

    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    public function getBthdate()
    {
        if($this->birthdate != '0000-00-00' && $this->birthdate){
            return date('d.m.Y',strtotime($this->birthdate));
        }
        return '';
    }
    public function upload()
    {
        $session = Yii::$app->session;
        $city = $session->get('city');
        $sport = $session->get('sport');
        $name_model = 'player';

        //msk/football/referee/

        $path = $city;
        if (!file_exists($path)) { // проверяем есть ли папка города
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport;
        if (!file_exists($path)) { // проверяем есть ли папка спорта в городе
            FileHelper::createDirectory($path);
        }

        $path = $city . '/' . $sport . '/' . $name_model;
        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }

        $photo = UploadedFile::getInstance($this, 'photo');
        if ($photo) {
            $tmp = $photo->tempName;
            $size = $photo->size;
            $fnTemp = explode(".", $photo->name);
            $fnTemp = end($fnTemp);
            $ext = strtolower($fnTemp); // расширение файла
            $name = md5($photo->name.$path).'.'.$ext;
            //$name = str_ireplace('.' . $ext, '', $photo->name);
            $file_path = $path . '/' . $name;
            $photo->saveAs($file_path);
            $this->photo = '/'.$file_path;
        }
        return true;
    }
}
