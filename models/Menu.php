<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property int $id
 * @property string $link
 * @property string $preg
 * @property string $title
 * @property string $css_class
 * @property int $parent_id
 * @property int $sort
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'preg', 'title', 'css_class', 'parent_id', 'sort'], 'required'],
            [['parent_id', 'sort'], 'integer'],
            [['link', 'preg', 'title'], 'string', 'max' => 100],
            [['css_class'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'preg' => 'Preg',
            'title' => 'Title',
            'css_class' => 'Css Class',
            'parent_id' => 'Parent ID',
            'sort' => 'Sort',
        ];
    }
}
