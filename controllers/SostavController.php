<?php

namespace app\controllers;

use app\models\Player;
use app\models\Team;
use Yii;
use app\models\TeamSostav;
use app\models\SostavSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SostavController implements the CRUD actions for TeamSostav model.
 */
class SostavController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'], //гость
                        'denyCallback' => function ($rule, $action) {
                            return Yii::$app->response->redirect('/login', 302)->send();
                        }
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'], //аутентифицированный
                        'denyCallback' => function ($rule, $action) {
                            return Yii::$app->response->redirect('/site/error', 302)->send();
                        }
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TeamSostav models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SostavSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $teams = Team::find()->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'teams' => $teams,
        ]);
    }

    /**
     * Displays a single TeamSostav model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TeamSostav model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TeamSostav();

        $teams_sostav = TeamSostav::find()->select(['team_id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();

        $teams = Team::find()->where([
            'not in',
            'id',
            $teams_sostav
        ])->all();

        if ($model->load(Yii::$app->request->post())) {
            $players = Yii::$app->request->post('Players', []);
            $revoked = Yii::$app->request->post('Revoked', []);
            $model->season_id = Yii::$app->session->get('season');
            $model->players = implode(',', $players);
            $model->revoked = implode(',', $revoked);

            $disc = Yii::$app->request->post('Disq');
            $administrations = Yii::$app->request->post('Administrations');
            if ($administrations) {
                foreach ($administrations as $key => $administration) {
                    $name = $administration['name'];
                    if (!$name) {
                        unset($administrations[$key]);
                        continue;
                    }
                }
            }
            $model->administration = serialize($administrations);
            $model->disc = serialize($disc);
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'teams' => $teams,
        ]);
    }

    /**
     * Updates an existing TeamSostav model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $teams = Team::find()->all();

        if ($model->load(Yii::$app->request->post())) {
            $players = Yii::$app->request->post('Players', []);
            $revoked = Yii::$app->request->post('Revoked', []);
            $model->players = implode(',', $players);
            $model->revoked = implode(',', $revoked);

            $disc = Yii::$app->request->post('Disq');
            $administrations = Yii::$app->request->post('Administrations');
            if ($administrations) {
                foreach ($administrations as $key => $administration) {
                    $name = $administration['name'];
                    if (!$name) {
                        unset($administrations[$key]);
                        continue;
                    }
                }
            }
            $model->administration = serialize($administrations);
            $model->disc = serialize($disc);
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'teams' => $teams,
        ]);
    }

    /**
     * Deletes an existing TeamSostav model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionPlayers($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $players = Player::find()->where(['team_id' => $id])->all();
        $sostav = [
            'players' => $this->renderPartial('_players', [
                'players' => $players,
                'players_enabled' => [],
                'revoked_enabled' => [],
            ]),
            'disq' => $this->renderPartial('_disc', [
                'players' => $players,
                'disqualifications' => '',
            ])
        ];

        return $sostav;
    }

    /**
     * Finds the TeamSostav model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TeamSostav the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TeamSostav::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
