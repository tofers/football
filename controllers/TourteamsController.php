<?php

namespace app\controllers;

use app\models\Division;
use app\models\Player;
use app\models\Team;
use app\models\TeamSostav;
use Yii;
use app\models\RatingsTourTeam;
use app\models\RatingsTourTeamSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TourteamsController implements the CRUD actions for RatingsTourTeam model.
 */
class TourteamsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'], //гость
                        'denyCallback' => function ($rule, $action) {
                            return Yii::$app->response->redirect('/login', 302)->send();
                        }
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'], //аутентифицированный
                        'denyCallback' => function ($rule, $action) {
                            return Yii::$app->response->redirect('/site/error', 302)->send();
                        }
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RatingsTourTeam models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RatingsTourTeamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $divisions = Division::find()->where([
            'season_id' => Yii::$app->session->get('season')
        ])->all();
        $teams_sostav = TeamSostav::find()->select(['team_id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();

        $teams = Team::find()->where([
            'id' => $teams_sostav
        ])->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'divisions' => $divisions,
            'teams' => $teams,
        ]);
    }

    /**
     * Displays a single RatingsTourTeam model.
     * @param integer $tour
     * @param integer $division_id
     * @param integer $position
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($tour, $division_id, $position)
    {
        return $this->render('view', [
            'model' => $this->findModel($tour, $division_id, $position),
        ]);
    }

    /**
     * Creates a new RatingsTourTeam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RatingsTourTeam();

        if (Yii::$app->request->isPost) {
            $model = new RatingsTourTeam();
            $model->load(Yii::$app->request->post());

            $tourteams = Yii::$app->request->post('Tourteams');

            foreach ($tourteams as $position => $tourteam) {
                $raiting = new RatingsTourTeam();
                $raiting->player_id = $tourteam['player_id'];
                $raiting->team_id = $tourteam['team_id'];
                $raiting->description = $tourteam['description'];
                $raiting->season_id = Yii::$app->session->get('season');
                $raiting->tour = $model->tour;
                $raiting->division_id = $model->division_id;
                $raiting->position = (string)$position;
                $raiting->save(false);
                if ($raiting->errors) {
                    print_r($raiting->errors);
                    die();
                }
            }
            return $this->redirect(['index']);
        }
        $divisions = Division::find()->where([
            'season_id' => Yii::$app->session->get('season')
        ])->all();
        $teams_sostav = TeamSostav::find()->select(['team_id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();

        $teams = Team::find()->where([
            'id' => $teams_sostav
        ])->all();

        return $this->render('create', [
            'model' => $model,
            'tourteam1' => $model,
            'tourteam2' => $model,
            'tourteam3' => $model,
            'tourteam4' => $model,
            'tourteam5' => $model,
            'tourteam6' => $model,
            'teams' => $teams,
            'divisions' => $divisions,
        ]);
    }

    /**
     * Updates an existing RatingsTourTeam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $tour
     * @param integer $division_id
     * @param integer $position
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($division_id, $tour)
    {
        $tourteam1 = $this->findModel($tour, $division_id, 1);
        $tourteam2 = $this->findModel($tour, $division_id, 2);
        $tourteam3 = $this->findModel($tour, $division_id, 3);
        $tourteam4 = $this->findModel($tour, $division_id, 4);
        $tourteam5 = $this->findModel($tour, $division_id, 5);
        $tourteam6 = $this->findModel($tour, $division_id, 6);


        if (Yii::$app->request->isPost) {
            $raitingstourteam = [
                1 => $tourteam1,
                2 => $tourteam2,
                3 => $tourteam3,
                4 => $tourteam4,
                5 => $tourteam5,
                6 => $tourteam6,
            ];

            $model = new RatingsTourTeam();
            $model->load(Yii::$app->request->post());

            $tourteams = Yii::$app->request->post('Tourteams');

            foreach ($tourteams as $position => $tourteam) {
                $raiting = $raitingstourteam[$position];
                $raiting->player_id = $tourteam['player_id'];
                $raiting->team_id = $tourteam['team_id'];
                $raiting->description = $tourteam['description'];
                $raiting->tour = $model->tour;
                $raiting->division_id = $model->division_id;
                $raiting->position = (string)$position;
                $raiting->save(false);
            }
            return $this->redirect(['index']);
        }


        $divisions = Division::find()->where([
            'season_id' => Yii::$app->session->get('season')
        ])->all();
        $teams_sostav = TeamSostav::find()->select(['team_id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();

        $teams = Team::find()->where([
            'id' => $teams_sostav
        ])->all();
        return $this->render('update', [
            'model' => $tourteam1,
            'tourteam1' => $tourteam1,
            'tourteam2' => $tourteam2,
            'tourteam3' => $tourteam3,
            'tourteam4' => $tourteam4,
            'tourteam5' => $tourteam5,
            'tourteam6' => $tourteam6,
            'teams' => $teams,
            'divisions' => $divisions,
        ]);
    }

    /**
     * Deletes an existing RatingsTourTeam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $tour
     * @param integer $division_id
     * @param integer $position
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($tour, $division_id, $position)
    {
        RatingsTourTeam::deleteAll([
            'tour' => $tour,
            'division_id' => $division_id,
        ]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the RatingsTourTeam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $tour
     * @param integer $division_id
     * @param integer $position
     * @return RatingsTourTeam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($tour, $division_id, $position)
    {
        if (($model = RatingsTourTeam::findOne(['tour' => $tour, 'division_id' => $division_id, 'position' => $position])) !== null) {
            return $model;
        }

        return new RatingsTourTeam();
    }

    public function actionPlayer($id, $key)
    {

        $teams_sostav = TeamSostav::find()->where([
            'season_id' => Yii::$app->session->get('season'),
            'team_id' => $id
        ])->one();
        $teams_sostav = explode(',', $teams_sostav->players);
        $players = Player::find()->where(['id' => $teams_sostav])->orderBy(['full_name' => SORT_ASC])->all();
        return $this->renderPartial('_players', [
            'players' => $players,
            'key' => $key,
        ]);
    }
}
