<?php

namespace app\controllers;

use app\models\Division;
use app\models\MatchPlayer;
use app\models\Referee;
use app\models\Team;
use app\models\TeamSostav;
use app\models\Video;
use Yii;
use app\models\Match;
use app\models\MatchSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MatchController implements the CRUD actions for Match model.
 */
class MatchesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => false,
                        'roles' => ['?'], //гость
                        'denyCallback' => function ($rule, $action) {
                            return Yii::$app->response->redirect('/login', 302)->send();
                        }
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'], //аутентифицированный
                        'denyCallback' => function ($rule, $action) {
                            return Yii::$app->response->redirect('/site/error', 302)->send();
                        }
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Match models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MatchSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $teams_sostav = TeamSostav::find()->select(['team_id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();

        $teams = Team::find()->where([
            'id' => $teams_sostav
        ])->all();

        $referees = Referee::find()->orderBy(['full_name' => SORT_ASC])->all();
        $divisions = Division::find()->where([
            'season_id' => Yii::$app->session->get('season')
        ])->all();
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'teams' => $teams,
            'divisions' => $divisions,
            'referees' => $referees,
        ]);
    }

    /**
     * Displays a single Match model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            MatchPlayer::deleteAll(['match_id' => $id]);
            $type_card = [
                1 => 'cards_yellow',
                2 => 'cards_2yellow',
                3 => 'cards_red',
            ];
            $goals1 = Yii::$app->request->post('Score' . $model->team1_id);
            $cards1 = Yii::$app->request->post('Card' . $model->team1_id);
            $penalties1 = Yii::$app->request->post('Penalties' . $model->team1_id);

            $players = Yii::$app->request->post('Players');
            $players1 = $players[$model->team1_id];
            $statistics1 = [];
            if ($goals1) {
                foreach ($goals1 as $key => $goal) {
                    $player_id = $goal['player_id'];
                    $assistent_id = $goal['assistent_id'];
                    if (!$player_id) {
                        unset($goals1[$key]);
                        continue;
                    }
                    if (!isset($statistics1[$assistent_id]) && $assistent_id) {
                        $statistics1[$assistent_id] = [];
                    }
                    if (!isset($statistics1[$player_id])) {
                        $statistics1[$player_id] = [];
                    }
                    if (isset($statistics1[$player_id])) {
                        if (!isset($statistics1[$player_id]['goals'])) {
                            $statistics1[$player_id]['goals'] = 0;
                        }
                        $statistics1[$player_id]['goals']++;
                    }
                    if (isset($statistics1[$assistent_id]) && $assistent_id) {
                        if (!isset($statistics1[$player_id]['assistent'])) {
                            $statistics1[$player_id]['assistent'] = 0;
                        }
                        $statistics1[$player_id]['assistent']++;
                    }
                }
            }
            if ($cards1) {
                foreach ($cards1 as $key => $card) {
                    $player_id = $card['player_id'];
                    $type = $card['type'];
                    if (!$player_id) {
                        unset($cards1[$key]);
                        continue;
                    }
                    if (!isset($statistics1[$player_id])) {
                        $statistics1[$player_id] = [];
                    }
                    if (isset($statistics1[$player_id])) {
                        if (!isset($statistics1[$player_id]['cards_yellow'])) {
                            $statistics1[$player_id]['cards_yellow'] = 0;
                        }
                        if (!isset($statistics1[$player_id]['cards_2yellow'])) {
                            $statistics1[$player_id]['cards_2yellow'] = 0;
                        }
                        if (!isset($statistics1[$player_id]['cards_red'])) {
                            $statistics1[$player_id]['cards_red'] = 0;
                        }
                        $statistics1[$player_id][$type_card[$type]]++;
                    }
                }
            }
            if ($penalties1) {
                foreach ($penalties1 as $key => $penalti) {
                    $player_id = $penalti['player_id'];
                    $result = $penalti['result'];
                    if (!$player_id) {
                        unset($penalties1[$key]);
                        continue;
                    }
                    if (!isset($statistics1[$player_id])) {
                        $statistics1[$player_id] = [];
                    }
                    if (isset($statistics1[$player_id]) && $result) {
                        if (!isset($statistics1[$player_id]['penalties'])) {
                            $statistics1[$player_id]['penalties'] = 0;
                        }
                        $statistics1[$player_id]['penalties']++;
                    }
                }
            }

            if ($players1) {
                foreach ($players1 as $player_id) {
                    $goals = isset($statistics1[$player_id]['goals']) ? $statistics1[$player_id]['goals'] : 0;
                    $cards_red = isset($statistics1[$player_id]['cards_red']) ? $statistics1[$player_id]['cards_red'] : 0;
                    $cards_yellow = isset($statistics1[$player_id]['cards_yellow']) ? $statistics1[$player_id]['cards_yellow'] : 0;
                    $cards_2yellow = isset($statistics1[$player_id]['cards_2yellow']) ? $statistics1[$player_id]['cards_2yellow'] : 0;
                    $penalties = isset($statistics1[$player_id]['penalties']) ? $statistics1[$player_id]['penalties'] : 0;
                    $assistent = isset($statistics1[$player_id]['assistent']) ? $statistics1[$player_id]['assistent'] : 0;

                    $match_player = new MatchPlayer();
                    $match_player->match_id = $id;
                    $match_player->team_id = $model->team1_id;
                    $match_player->player_id = $player_id;
                    $match_player->goals = $goals;
                    $match_player->penalties = $penalties;
                    $match_player->assistent = $assistent;
                    $match_player->cards_yellow = $cards_yellow;
                    $match_player->cards_2yellow = $cards_2yellow;
                    $match_player->cards_red = $cards_red;
                    $match_player->cards_ball = $cards_yellow + $cards_2yellow + $cards_red * 3;
                    $match_player->save();
                    if ($match_player->errors) {
                        print_r($match_player->errors);
                        die();
                    }
                }
            }


            $model->team1_score = count($goals1);
            $model->team1_goals_array = serialize($goals1);
            $model->team1_cards_array = serialize($cards1);
            $model->team1_penalties_array = serialize($penalties1);
            $model->team1_penalties = count($penalties1);


            $goals2 = Yii::$app->request->post('Score' . $model->team2_id);
            $cards2 = Yii::$app->request->post('Card' . $model->team2_id);
            $penalties2 = Yii::$app->request->post('Penalties' . $model->team2_id);

            $players2 = $players[$model->team2_id];
            $statistics2 = [];
            if ($goals2) {
                foreach ($goals2 as $key => $goal) {
                    $player_id = $goal['player_id'];
                    $assistent_id = $goal['assistent_id'];
                    if (!$player_id) {
                        unset($goals2[$key]);
                        continue;
                    }
                    if (!isset($statistics2[$assistent_id]) && $assistent_id) {
                        $statistics2[$assistent_id] = [];
                    }
                    if (!isset($statistics2[$player_id])) {
                        $statistics2[$player_id] = [];
                    }
                    if (isset($statistics2[$player_id])) {
                        if (!isset($statistics2[$player_id]['goals'])) {
                            $statistics2[$player_id]['goals'] = 0;
                        }
                        $statistics2[$player_id]['goals']++;
                    }
                    if (isset($statistics2[$assistent_id]) && $assistent_id) {
                        if (!isset($statistics2[$player_id]['assistent'])) {
                            $statistics2[$player_id]['assistent'] = 0;
                        }
                        $statistics2[$player_id]['assistent']++;
                    }
                }
            }
            if ($cards2) {
                foreach ($cards2 as $key => $card) {
                    $player_id = $card['player_id'];
                    $type = $card['type'];
                    if (!$player_id) {
                        unset($cards2[$key]);
                        continue;
                    }
                    if (!isset($statistics2[$player_id])) {
                        $statistics2[$player_id] = [];
                    }
                    if (isset($statistics2[$player_id])) {
                        if (!isset($statistics2[$player_id]['cards_yellow'])) {
                            $statistics2[$player_id]['cards_yellow'] = 0;
                        }
                        if (!isset($statistics2[$player_id]['cards_2yellow'])) {
                            $statistics2[$player_id]['cards_2yellow'] = 0;
                        }
                        if (!isset($statistics2[$player_id]['cards_red'])) {
                            $statistics2[$player_id]['cards_red'] = 0;
                        }
                        $statistics2[$player_id][$type_card[$type]]++;
                    }
                }
            }
            if ($penalties2) {
                foreach ($penalties2 as $key => $penalti) {
                    $player_id = $penalti['player_id'];
                    $result = $penalti['result'];
                    if (!$player_id) {
                        unset($penalties2[$key]);
                        continue;
                    }
                    if (!isset($statistics2[$player_id])) {
                        $statistics2[$player_id] = [];
                    }
                    if (isset($statistics2[$player_id]) && $result) {
                        if (!isset($statistics2[$player_id]['penalties'])) {
                            $statistics2[$player_id]['penalties'] = 0;
                        }
                        $statistics2[$player_id]['penalties']++;
                    }
                }
            }

            if ($players2) {
                foreach ($players2 as $player_id) {
                    $goals = isset($statistics2[$player_id]['goals']) ? $statistics2[$player_id]['goals'] : 0;
                    $cards_red = isset($statistics2[$player_id]['cards_red']) ? $statistics2[$player_id]['cards_red'] : 0;
                    $cards_yellow = isset($statistics2[$player_id]['cards_yellow']) ? $statistics2[$player_id]['cards_yellow'] : 0;
                    $cards_2yellow = isset($statistics2[$player_id]['cards_2yellow']) ? $statistics2[$player_id]['cards_2yellow'] : 0;
                    $penalties = isset($statistics1[$player_id]['penalties']) ? $statistics1[$player_id]['penalties'] : 0;
                    $assistent = isset($statistics1[$player_id]['assistent']) ? $statistics1[$player_id]['assistent'] : 0;

                    $match_player = new MatchPlayer();
                    $match_player->match_id = $id;
                    $match_player->team_id = $model->team2_id;
                    $match_player->player_id = $player_id;
                    $match_player->goals = $goals;
                    $match_player->penalties = $penalties;
                    $match_player->assistent = $assistent;
                    $match_player->cards_yellow = $cards_yellow;
                    $match_player->cards_2yellow = $cards_2yellow;
                    $match_player->cards_red = $cards_red;
                    $match_player->cards_ball = $cards_yellow + $cards_2yellow + $cards_red * 3;
                    $match_player->save();
                }
            }

            $model->team2_score = count($goals2);
            $model->team2_goals_array = serialize($goals2);
            $model->team2_cards_array = serialize($cards2);
            $model->team2_penalties_array = serialize($penalties2);
            $model->team2_penalties = count($penalties2);
            if ($model->save(false)) {
                return $this->redirect(['index']);
            }
        }


        $referees = Referee::find()->orderBy(['full_name' => SORT_ASC])->all();
        return $this->render('view', [
            'model' => $model,
            'referees' => $referees,
        ]);
    }

    /**
     * Creates a new Match model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Match();

        if ($model->load(Yii::$app->request->post())) {
            $model->date = $model->date ? date('Y-m-d H:i:s', strtotime($model->date)) : '';

            $model->team1_name = $model->team1->name;
            $model->team2_name = $model->team2->name;
            $model->stadium = $model->stadium ? $model->stadium : 0;
            $model->referee_id = $model->referee_id ? $model->referee_id : 0;
            $model->referee2_id = $model->referee2_id ? $model->referee2_id : 0;
            $model->video_id = $model->video_id ? $model->video_id : 0;
            if ($model->save(false)) {
                return $this->redirect(['index']);
            }
        }

        $teams_sostav = TeamSostav::find()->select(['team_id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();

        $teams = Team::find()->where([
            'id' => $teams_sostav
        ])->all();
        $referees = Referee::find()->orderBy(['full_name' => SORT_ASC])->all();
        $videos = Video::find()->all();
        $divisions = Division::find()->where([
            'season_id' => Yii::$app->session->get('season')
        ])->all();
        return $this->render('create', [
            'model' => $model,
            'teams' => $teams,
            'divisions' => $divisions,
            'referees' => $referees,
            'videos' => $videos,
        ]);
    }

    /**
     * Updates an existing Match model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $model->date = $model->date ? date('Y-m-d H:i:s', strtotime($model->date)) : '';
            $model->team1_name = $model->team1->name;
            $model->team2_name = $model->team2->name;

            $model->referee_id = $model->referee_id ? $model->referee_id : 0;
            $model->referee2_id = $model->referee2_id ? $model->referee2_id : 0;
            $model->video_id = $model->video_id ? $model->video_id : 0;
            $model->stadium = $model->stadium ? $model->stadium : 0;
            if ($model->save(false)) {
                return $this->redirect(['index']);
            }
        }

        $teams_sostav = TeamSostav::find()->select(['team_id'])->where([
            'season_id' => Yii::$app->session->get('season')
        ])->column();

        $teams = Team::find()->where([
            'id' => $teams_sostav
        ])->all();
        $referees = Referee::find()->orderBy(['full_name' => SORT_ASC])->all();
        $videos = Video::find()->all();
        $divisions = Division::find()->where([
            'season_id' => Yii::$app->session->get('season')
        ])->all();
        return $this->render('update', [
            'model' => $model,
            'teams' => $teams,
            'divisions' => $divisions,
            'referees' => $referees,
            'videos' => $videos,
        ]);
    }

    /**
     * Deletes an existing Match model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Match model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Match the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Match::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
