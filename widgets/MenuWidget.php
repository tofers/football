<?php

namespace app\widgets;

use yii\bootstrap\Nav;
use yii\widgets\Menu;

class MenuWidget extends \yii\bootstrap\Widget
{
    /**
     * Запуск виджета
     */
    public function run()
    {
        $menu_items = [];
        $menu_items[] = array(
            'label' => '<i class="icon-home"></i><span class="title">Главная</span>',
            'url' => array('/site'),
            'active' => (\Yii::$app->controller->id == 'site'),
            'visible' => false,
        );
        $menu_items[] = array(
            'label' => '<i class="icon-globe-alt"></i><span class="title"> Города</span>',
            'url' => array('/city'),
            'active' => (\Yii::$app->controller->id == 'city'),
            'visible' => false,
        );
        $menu_items[] = array(
            'label' => '<i class="fa fa-soccer-ball-o"></i><span class="title"> Матчи</span>',
            'url' => array('/matches'),
            'active' => (\Yii::$app->controller->id == 'matches'),
        );
        $menu_items[] = array(
            'label' => '<i class="icon-map"></i><span class="title"> Турниры</span><span class="arrow"></span>',
            'url' => array('javascript:;'),
            'active' => in_array(\Yii::$app->controller->id, ['season', 'division', 'tourteams']),
            'items' =>
                [
                    [
                        'label' => '<span class="title"> Сезоны</span>',
                        'url' => array('/season'),
                        'active' => (\Yii::$app->controller->id == 'season'),
                    ],
                    [
                        'label' => '<span class="title"> Дивизионы</span>',
                        'url' => array('/division'),
                        'active' => (\Yii::$app->controller->id == 'division'),
                    ],
                    [
                        'label' => '<span class="title"> Сборные туров</span>',
                        'url' => array('/tourteams'),
                        'active' => (\Yii::$app->controller->id == 'tourteams'),
                    ]
                ]
        );

        $menu_items[] = array(
            'label' => '<i class="fa fa-users"></i><span class="title"> Составы команд</span>',
            'url' => array('/sostav'),
            'active' => (\Yii::$app->controller->id == 'sostav'),
        );


        $menu_items[] = array(
            'label' => '<i class="icon-users"></i><span class="title"> Команды</span>',
            'url' => array('/team'),
            'active' => (\Yii::$app->controller->id == 'team'),
        );
        $menu_items[] = array(
            'label' => '<i class="icon-user"></i><span class="title"> Игроки</span>',
            'url' => array('/player'),
            'active' => (\Yii::$app->controller->id == 'player'),
        );
        $menu_items[] = array(
            'label' => '<i class="fa fa-user-secret"></i><span class="title"> Судьи</span>',
            'url' => array('/referee'),
            'active' => (\Yii::$app->controller->id == 'referee'),
        );
        $menu_items[] = array(
            'label' => '<i class="fa fa-suitcase"></i><span class="title"> Партнеры</span>',
            'url' => array('/partner'),
            'active' => (\Yii::$app->controller->id == 'partner'),
        );

        $menu_items[] = array(
            'label' => '<i class="icon-camera"></i><span class="title">Галерея</span>',
            'url' => ['/gallery'],
            'active' => (\Yii::$app->controller->id == 'gallery'),

        );
        $menu_items[] = array(
            'label' => '<i class="icon-camcorder"></i><span class="title">Видео</span>',
            'url' => ['/video'],
            'active' => (\Yii::$app->controller->id == 'video'),
        );

        $menu_items[] = array(
            'label' => '<i class="icon-picture"></i><span class="title">Баннеры</span>',
            'url' => ['/banner'],
            'active' => (\Yii::$app->controller->id == 'banner'),
        );

        $menu_items[] = array(
            'label' => '<i class="fa fa-newspaper-o"></i><span class="title"> Пресса</span><span class="arrow"></span>',
            'url' => ['javascript:;'],
            'active' => in_array(\Yii::$app->controller->id, ['news', 'media', 'poll','runstring']),
            'items' => [
                [
                    'label' => '</i><span class="title"> Новости</span>',
                    'url' => ['/news'],
                    'active' => (\Yii::$app->controller->id == 'news'),
                ],
                [
                    'label' => '</i><span class="title"> СМИ о нас</span>',
                    'url' => ['/media'],
                    'active' => (\Yii::$app->controller->id == 'media'),
                ],
                [
                    'label' => '<span class="title"> Голосования</span>',
                    'url' => ['/poll'],
                    'active' => (\Yii::$app->controller->id == 'poll'),
                ],
                [
                    'label' => '<span class="title"> Бегущая строка</span>',
                    'url' => ['/runstring'],
                    'active' => (\Yii::$app->controller->id == 'runstring'),
                ],

            ]
        );
        echo Menu::widget([
            'itemOptions' => [
                'class' => 'nav-item',
            ],
            'options' => [
                'class' => 'page-sidebar-menu',
                'data-keep-expanded' => 'false',
                'data-auto-scroll' => 'true',
                'data-slide-speed' => '200'
            ],
            'submenuTemplate' => '<ul role="menu" class="sub-menu">{items}</ul>' . "\n",
            'items' => $menu_items,
            'encodeLabels' => false,
        ]);
    }
}