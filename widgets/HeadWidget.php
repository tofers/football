<?php

namespace app\widgets;

use app\models\Season;
use Yii;

class HeadWidget extends \yii\bootstrap\Widget
{
    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $seasons = Season::find()->orderBy(['start_date' => SORT_DESC])->all();
        $season = Season::find()->orderBy(['start_date' => SORT_DESC])->one();
        $season_last_id = '';
        if ($season) {
            $season_last_id = $season->id;
        }
        return $this->render('head', [
            'seasons' => $seasons,
            'season_last_id' => $season_last_id
        ]);
    }
}
