<?

/* @var  $seasons \app\models\Season[] */

/* @var  $season_last_id int */

use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;

$session = Yii::$app->session;
if (!$session->isActive) {
    $session->open();
}
$cities = [
    'spb' => 'Санкт-Петербург',
    'krn' => 'Красноярск',
    'vrn' => 'Воронеж',
    'msk' => 'Москва',
    'sochi' => 'Сочи',
];
$sports = [
    'football' => 'Футбол',
    'basketball' => 'Баскетбол',
    'volleyball' => 'Волейбол',
    'tennis' => 'Теннис',
    'cheerleading' => 'Черлидинг',
    'esport' => 'Киберспорт',
    'bowling' => 'Боулинг',
    'hockey' => 'Хоккей',
];
if (!$session->has('city')) {
    $session->set('city', 'msk');
}
if (!$session->has('sport')) {
    $session->set('sport', 'football');
}
$city = $session->get('city', 'msk');
$sport = $session->get('sport', 'football');

$season = $session->get('season', $season_last_id);
$ss_ids = ArrayHelper::map($seasons, 'id', 'id');
if (!$session->has('season') || !isset($ss_ids[$season])) {
    $season = $season_last_id;
    $session->set('season', $season);
}

$seasons = ArrayHelper::map($seasons, 'id', 'title', 'year');

?>

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/">
                <img src="/img/logo_main.png" alt="logo" class="logo-default logo-lcb"/> </a>
            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse"> </a>
        <div class="page-top">
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown dropdown-notification" id="header_city">
                        <?= Html::dropDownList('city', $city, $cities, [
                            'class' => 'selectpicker form-control input-small'
                        ]) ?>
                    </li>
                    <li class="dropdown dropdown-notification hidden" id="header_sport">
                        <?= Html::dropDownList('sport', $sport, $sports, [
                            'class' => 'selectpicker form-control input-small'
                        ]) ?>
                    </li>
                    <li class="dropdown dropdown-notification" id="header_season">
                        <?= Html::dropDownList('season', $season, $seasons, [
                            'class' => 'selectpicker form-control input-small',
                            'data-live-search' => 'true'
                        ]) ?>
                    </li>
                    <li class="separator hide"></li>
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <span class="username username-hide-on-mobile"> <?= Yii::$app->user->identity->username ?> </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <img alt="" class="img-circle" src="/assets/layouts/layout4/img/avatar9.jpg"/> </a>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <li class="dropdown dropdown-extended dropdown-tasks">
                        <span class="sr-only">Toggle Quick Sidebar</span>
                        <a href="/logout" class="logout dropdown-toggle"><i class="icon-logout"></i></a>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->