<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LoginAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'assets/global/plugins/bootstrap-select/css/bootstrap-select.css',
        'assets/global/css/components.min.css',
        'assets/global/css/plugins.min.css',
        'assets/layouts/layout4/css/layout.min.css',
        'assets/layouts/layout4/css/themes/default.min.css',
        'assets/layouts/layout4/css/custom.min.css',
        'assets/pages/css/login-4.css',
        'css/site.css',
    ];
    public $js = [
        '//code.jquery.com/jquery-migrate-3.0.1.js',
        'assets/global/plugins/js.cookie.min.js',
        'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
        'assets/global/scripts/app.min.js',
        'assets/layouts/layout4/scripts/layout.min.js',
        'assets/layouts/layout4/scripts/demo.min.js',
        'js/login.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
