<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'assets/global/plugins/datatables/datatables.min.css',
        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
        'assets/global/plugins/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        'assets/global/plugins/bootstrap-toastr/toastr.min.css',
        'assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css',
        'assets/global/plugins/bootstrap-select/css/bootstrap-select.css',
        'assets/global/plugins/select2/css/select2.min.css',
        'assets/global/plugins/select2/css/select2-bootstrap.min.css',
        'assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css',
        'assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css',
        'assets/global/plugins/bootstrap-summernote/summernote.css',
        'assets/global/css/components.min.css',
        'assets/global/css/plugins.min.css',

        'assets/layouts/layout4/css/layout.min.css',
        'assets/layouts/layout4/css/themes/default.min.css',
        'assets/layouts/layout4/css/custom.min.css',
        'css/site.css',
    ];
    public $js = [
        '//code.jquery.com/jquery-migrate-3.0.1.js',
        'assets/global/plugins/js.cookie.min.js',
        'assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'assets/global/plugins/jquery.blockui.min.js',
        'assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'assets/global/plugins/counterup/jquery.counterup.min.js',
        'assets/global/plugins/counterup/jquery.waypoints.min.js',
        'assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js',
        'assets/global/plugins/typeahead/handlebars.min.js',
        'assets/global/plugins/typeahead/typeahead.bundle.min.js',
        'assets/global/plugins/moment/moment.js',
        'assets/global/plugins/moment/locale/ru.js',
        'assets/global/plugins/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        'assets/global/plugins/bootstrap-toastr/toastr.min.js',
        'assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
        'assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
        'assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js',
        'assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
        'assets/global/plugins/select2/js/select2.full.min.js',
        'assets/global/plugins/bootstrap-summernote/summernote.min.js',
        'assets/global/plugins/jquery-repeater/jquery.repeater.js',
        'assets/global/scripts/app.min.js',
        'assets/layouts/layout4/scripts/layout.min.js',
        'assets/layouts/layout4/scripts/demo.min.js',
        'assets/layouts/global/scripts/quick-sidebar.min.js',
        'assets/layouts/global/scripts/quick-nav.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
