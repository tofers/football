<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Team */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Команды', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-view">


    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>  Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-circle']) ?>
        <?= Html::a('<i class="fa fa-trash"></i>  Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'company',
            'description:ntext',
            'text:ntext',
            'site',
            'facebook',
            'twitter',
            'vkontakte',
            'trainer',
            [
                'attribute' => 'photo',
                'format' => ['image',['width'=>'100']],
            ],
            [
                'attribute' => 'logo',
                'format' => ['image',['width'=>'80']],
            ],
            [
                'attribute' => 'mini_logo',
                'format' => ['image',['width'=>'26']],
            ],
        ],
    ]) ?>

</div>
