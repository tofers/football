<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-users font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                    <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="team-form">

                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data'
                        ]
                    ]); ?>
                    <div class="row">

                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 ">

                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'company')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'vkontakte')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>


                            <?= $form->field($model, 'trainer')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

                            <?= $form->field($model, 'text')->textarea(['rows' => 6, 'class' => 'form-control summernote']) ?>

                            <div class="form-group">
                                <?= Html::activeLabel($model, 'photo', ['class' => 'control-label']) ?>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <div class="fileinput fileinput-<?= $model->photo ? 'exists' : 'new' ?>"
                                         data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 325px; height: 425px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 325px; max-height: 425px;">
                                            <img id="img-logo" src="<?= $model->photo ?>" alt=""
                                                 style="max-height: 425px;"/>
                                        </div>
                                        <div>
                                    <span class="btn blue btn-circle btn-file">
                                        <span class="fileinput-new  "> Загрузить </span>
                                        <span class="fileinput-exists"> Изменить </span>
                                        <input type="file" class="blue  btn btn-circle" name="Team[photo]"
                                               accept="image/*">
                                    </span>
                                            <?= Html::a('Удалить', 'javascript:;', [
                                                'class' => 'btn red fileinput-exists btn-circle',
                                                'data-dismiss' => 'fileinput'
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <?= Html::activeLabel($model, 'logo', ['class' => 'control-label']) ?>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <div class="fileinput fileinput-<?= $model->logo ? 'exists' : 'new' ?>"
                                         data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 192px; height: 192px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 192px; max-height: 192px;">
                                            <img id="img-logo" src="<?= $model->logo ?>" alt=""
                                                 style="max-height: 192px;"/>
                                        </div>
                                        <div>
                                    <span class="btn blue btn-circle btn-file">
                                        <span class="fileinput-new  "> Загрузить </span>
                                        <span class="fileinput-exists"> Изменить </span>
                                        <input type="file" class="blue  btn btn-circle" name="Team[logo]"
                                               accept="image/*">
                                    </span>
                                            <?= Html::a('Удалить', 'javascript:;', [
                                                'class' => 'btn red fileinput-exists btn-circle',
                                                'data-dismiss' => 'fileinput'
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Html::activeLabel($model, 'mini_logo', ['class' => 'control-label']) ?>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <div class="fileinput fileinput-<?= $model->mini_logo ? 'exists' : 'new' ?>"
                                         data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 26px; height: 26px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 26px; max-height: 26px;">
                                            <img id="img-logo" src="<?= $model->mini_logo ?>" alt=""
                                                 style="max-height: 26px;"/>
                                        </div>
                                        <div>
                                    <span class="btn blue btn-circle btn-file">
                                        <span class="fileinput-new  "> Загрузить </span>
                                        <span class="fileinput-exists"> Изменить </span>
                                        <input type="file" class="blue  btn btn-circle" name="Team[mini_logo]"
                                               accept="image/*">
                                    </span>
                                            <?= Html::a('Удалить', 'javascript:;', [
                                                'class' => 'btn red fileinput-exists btn-circle',
                                                'data-dismiss' => 'fileinput'
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>