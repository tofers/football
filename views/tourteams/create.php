<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RatingsTourTeam */
/* @var $tourteam1 app\models\RatingsTourTeam */
/* @var $tourteam2 app\models\RatingsTourTeam */
/* @var $tourteam3 app\models\RatingsTourTeam */
/* @var $tourteam4 app\models\RatingsTourTeam */
/* @var $tourteam5 app\models\RatingsTourTeam */
/* @var $tourteam6 app\models\RatingsTourTeam */

$this->title = 'Создать сборную тура';
$this->params['breadcrumbs'][] = ['label' => 'Сборные тура', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ratings-tour-team-create">


    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
        'divisions' => $divisions,
        'tourteam1' => $tourteam1,
        'tourteam2' => $tourteam2,
        'tourteam3' => $tourteam3,
        'tourteam4' => $tourteam4,
        'tourteam5' => $tourteam5,
        'tourteam6' => $tourteam6,
    ]) ?>

</div>
