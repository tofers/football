<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RatingsTourTeam */

$this->title = $model->tour;
$this->params['breadcrumbs'][] = ['label' => 'Сборные туров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ratings-tour-team-view">


    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>  Редактирование', ['update', 'tour' => $model->tour, 'division_id' => $model->division_id, 'position' => $model->position], ['class' => 'btn btn-primary btn-circle']) ?>
        <?= Html::a('<i class="fa fa-trash"></i>  Удалить', ['delete', 'tour' => $model->tour, 'division_id' => $model->division_id, 'position' => $model->position], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tour',
            'division.title',
            'position',
            'season_id',
            'team.name',
            'player.full_name',
            'description:ntext',
        ],
    ]) ?>

</div>
