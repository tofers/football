<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\RatingsTourTeam */
/* @var $form yii\widgets\ActiveForm */
$players=ArrayHelper::map($players, 'id','full_name');
echo  Html::dropDownList('Tourteams['.$key.'][player_id]', '', $players, [
    'class' => 'form-control selectpicker',
    'data-live-search' => 'true',
    'data-size' => '10',
    'prompt' => 'Выберите игрока'
]);
?>


