<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\RatingsTourTeam */
/* @var $form yii\widgets\ActiveForm */
/* @var $tourteam1 app\models\RatingsTourTeam */
/* @var $tourteam2 app\models\RatingsTourTeam */
/* @var $tourteam3 app\models\RatingsTourTeam */
/* @var $tourteam4 app\models\RatingsTourTeam */
/* @var $tourteam5 app\models\RatingsTourTeam */
/* @var $tourteam6 app\models\RatingsTourTeam */
$teams = ArrayHelper::map($teams, 'id', 'name');
$divisions = ArrayHelper::map($divisions, 'id', 'title');
$tour = [];
for ($i = 1; $i < 26; $i++) {
    $tour[$i] = 'Тур ' . $i;
}

$players1 = $tourteam1->isNewRecord ? [] : ArrayHelper::map($tourteam1->team->playerssostav,'id','full_name');
$players2 = $tourteam2->isNewRecord ? [] : ArrayHelper::map($tourteam2->team->playerssostav,'id','full_name');
$players3 = $tourteam3->isNewRecord ? [] : ArrayHelper::map($tourteam3->team->playerssostav,'id','full_name');
$players4 = $tourteam4->isNewRecord ? [] : ArrayHelper::map($tourteam4->team->playerssostav,'id','full_name');
$players5 = $tourteam5->isNewRecord ? [] : ArrayHelper::map($tourteam5->team->playerssostav,'id','full_name');
$players6 = $tourteam6->isNewRecord ? [] : ArrayHelper::map($tourteam6->team->playerssostav,'id','full_name');

?>


    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-map font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="ratings-tour-team-form">

                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                        <div class="row">
                            <div class="col-lg-6">

                                <?= $form->field($model, 'division_id')->dropDownList($divisions, [
                                    'class' => 'form-control selectpicker',
                                    'data-live-search' => 'true',
                                    'prompt' => 'Выберите дивизион'
                                ]) ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($model, 'tour')->dropDownList($tour, [
                                    'class' => 'form-control selectpicker',
                                    'data-live-search' => 'true',
                                    'prompt' => 'Выберите тур'
                                ]) ?>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group field-ratingstourteam-position">
                                    <label class="control-label" for="ratingstourteam-position">Команда</label>
                                    <?= Html::dropDownList('Tourteams[1][team_id]', $tourteam1->team_id, $teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите команду',
                                        'data-key' => '1',
                                    ]) ?>

                                    <div class="help-block">
                                    </div>
                                </div>

                                <div class="player1">
                                    <div class="form-group field-ratingstourteam-position">
                                        <label class="control-label"
                                               for="ratingstourteam-position">Игрок</label>
                                        <div id="player1">
                                            <?= Html::dropDownList('Tourteams[1][player_id]', $tourteam1->player_id, $players1, [
                                                'class' => 'form-control selectpicker',
                                                'data-live-search' => 'true',
                                                'data-size' => '10',
                                                'prompt' => 'Выберите игрока'
                                            ]) ?>
                                        </div>

                                        <div class="help-block">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group field-ratingstourteam-description required">
                                    <label class="control-label"
                                           for="ratingstourteam-description">Описание</label>
                                    <?= Html::textarea('Tourteams[1][description]', $tourteam1->description, [
                                        'class' => 'form-control ',
                                        'rows' => '6',
                                    ]) ?>
                                    <div class="help-block"></div>
                                </div>

                            </div>
                            <div class="col-lg-2">

                            </div>
                            <div class="col-lg-5">
                                <div class="form-group field-ratingstourteam-position">
                                    <label class="control-label" for="ratingstourteam-position">Команда</label>
                                    <?= Html::dropDownList('Tourteams[2][team_id]', $tourteam2->team_id, $teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите команду',
                                        'data-key' => '2',
                                    ]) ?>

                                    <div class="help-block">
                                    </div>
                                </div>

                                <div class="player2">
                                    <div class="form-group field-ratingstourteam-position">
                                        <label class="control-label"
                                               for="ratingstourteam-position">Игрок</label>
                                        <div id="player2">
                                            <?= Html::dropDownList('Tourteams[2][player_id]', $tourteam2->player_id, $players2, [
                                                'class' => 'form-control selectpicker',
                                                'data-live-search' => 'true',
                                                'data-size' => '10',
                                                'prompt' => 'Выберите игрока'
                                            ]) ?>
                                        </div>

                                        <div class="help-block">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group field-ratingstourteam-description required">
                                    <label class="control-label"
                                           for="ratingstourteam-description">Описание</label>
                                    <?= Html::textarea('Tourteams[2][description]', $tourteam2->description, [
                                        'class' => 'form-control ',
                                        'rows' => '6',
                                    ]) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group field-ratingstourteam-position">
                                    <label class="control-label" for="ratingstourteam-position">Команда</label>
                                    <?= Html::dropDownList('Tourteams[3][team_id]', $tourteam3->team_id, $teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите команду',
                                        'data-key' => '3',
                                    ]) ?>

                                    <div class="help-block">
                                    </div>
                                </div>

                                <div class="player3">
                                    <div class="form-group field-ratingstourteam-position">
                                        <label class="control-label"
                                               for="ratingstourteam-position">Игрок</label>
                                        <div id="player3">
                                            <?= Html::dropDownList('Tourteams[3][player_id]', $tourteam3->player_id, $players3, [
                                                'class' => 'form-control selectpicker',
                                                'data-live-search' => 'true',
                                                'data-size' => '10',
                                                'prompt' => 'Выберите игрока'
                                            ]) ?>
                                        </div>

                                        <div class="help-block">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group field-ratingstourteam-description required">
                                    <label class="control-label"
                                           for="ratingstourteam-description">Описание</label>
                                    <?= Html::textarea('Tourteams[3][description]', $tourteam3->description, [
                                        'class' => 'form-control ',
                                        'rows' => '6',
                                    ]) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-3">

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-5">
                                <div class="form-group field-ratingstourteam-position">
                                    <label class="control-label" for="ratingstourteam-position">Команда</label>
                                    <?= Html::dropDownList('Tourteams[4][team_id]', $tourteam4->team_id, $teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите команду',
                                        'data-key' => '4',
                                    ]) ?>

                                    <div class="help-block">
                                    </div>
                                </div>

                                <div class="player4">
                                    <div class="form-group field-ratingstourteam-position">
                                        <label class="control-label"
                                               for="ratingstourteam-position">Игрок</label>
                                        <div id="player4">
                                            <?= Html::dropDownList('Tourteams[4][player_id]', $tourteam4->player_id, $players4, [
                                                'class' => 'form-control selectpicker',
                                                'data-live-search' => 'true',
                                                'data-size' => '10',
                                                'prompt' => 'Выберите игрока'
                                            ]) ?>
                                        </div>

                                        <div class="help-block">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group field-ratingstourteam-description required">
                                    <label class="control-label"
                                           for="ratingstourteam-description">Описание</label>
                                    <?= Html::textarea('Tourteams[4][description]', $tourteam4->description, [
                                        'class' => 'form-control ',
                                        'rows' => '6',
                                    ]) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-2">

                            </div>
                            <div class="col-lg-5">
                                <div class="form-group field-ratingstourteam-position">
                                    <label class="control-label" for="ratingstourteam-position">Команда</label>
                                    <?= Html::dropDownList('Tourteams[5][team_id]', $tourteam5->team_id, $teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите команду',
                                        'data-key' => '5',
                                    ]) ?>

                                    <div class="help-block">
                                    </div>
                                </div>

                                <div class="player5">
                                    <div class="form-group field-ratingstourteam-position">
                                        <label class="control-label"
                                               for="ratingstourteam-position">Игрок</label>
                                        <div id="player5">
                                            <?= Html::dropDownList('Tourteams[5][player_id]', $tourteam5->player_id, $players5, [
                                                'class' => 'form-control selectpicker',
                                                'data-live-search' => 'true',
                                                'data-size' => '10',
                                                'prompt' => 'Выберите игрока'
                                            ]) ?>
                                        </div>

                                        <div class="help-block">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group field-ratingstourteam-description required">
                                    <label class="control-label"
                                           for="ratingstourteam-description">Описание</label>
                                    <?= Html::textarea('Tourteams[5][description]', $tourteam5->description, [
                                        'class' => 'form-control ',
                                        'rows' => '6',
                                    ]) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">

                            </div>
                            <div class="col-lg-6">
                                <div class="form-group field-ratingstourteam-position">
                                    <label class="control-label" for="ratingstourteam-position">Команда</label>
                                    <?= Html::dropDownList('Tourteams[6][team_id]', $tourteam6->team_id, $teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите команду',
                                        'data-key' => '6',
                                    ]) ?>

                                    <div class="help-block">
                                    </div>
                                </div>

                                <div class="player6">
                                    <div class="form-group field-ratingstourteam-position">
                                        <label class="control-label"
                                               for="ratingstourteam-position">Игрок</label>
                                        <div id="player6">
                                            <?= Html::dropDownList('Tourteams[6][player_id]', $tourteam6->player_id, $players6, [
                                                'class' => 'form-control selectpicker',
                                                'data-live-search' => 'true',
                                                'data-size' => '10',
                                                'prompt' => 'Выберите игрока'
                                            ]) ?>
                                        </div>

                                        <div class="help-block">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group field-ratingstourteam-description required">
                                    <label class="control-label"
                                           for="ratingstourteam-description">Описание</label>
                                    <?= Html::textarea('Tourteams[6][description]', $tourteam6->description, [
                                        'class' => 'form-control ',
                                        'rows' => '6',
                                    ]) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-3">

                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group hidden">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <div class="portlet-title portlet-footer">
            <div class="caption">
                <i class="fa fa-soccer-ball-o font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
    </div>

<?php
$this->registerJsFile('/js/tourteams.js', [
    'depends' => ['app\assets\AppAsset'],
    'position' => yii\web\View::POS_END
]);