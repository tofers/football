<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\RatingsTourTeamSearch */
/* @var $form yii\widgets\ActiveForm */
$divisions=ArrayHelper::map($divisions,'id','title');
$teams = ArrayHelper::map($teams, 'id', 'name');

$tour = [];
for ($i = 1; $i < 26; $i++) {
    $tour[$i] = 'Тур ' . $i;
}
?>

<div class="ratings-tour-team-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-6 col-lg-3">
            <?= $form->field($model, 'division_id')->dropDownList($divisions, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'data-size' => '10',
                'prompt' => 'Все'
            ]) ?>
        </div>
        <div class="col-md-4 col-lg-2">
            <?= $form->field($model, 'tour')->dropDownList($tour, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'prompt' => 'Все'
            ]) ?>
        </div>
        <div class="col-md-6 col-lg-3">
            <?php echo $form->field($model, 'team_id')->dropDownList($teams, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'data-size' => '10',
                'prompt' => 'Все команды'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="">&nbsp;</label>
                <div>
                    <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary btn-circle']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
