<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Runstring */

$this->title = 'Редактирование бегущей строки';
$this->params['breadcrumbs'][] = ['label' => 'Бегущая строка', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="runstring-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
