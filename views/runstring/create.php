<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Runstring */

$this->title = 'Создание бегущей строки';
$this->params['breadcrumbs'][] = ['label' => 'Бегущая строка', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="runstring-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
