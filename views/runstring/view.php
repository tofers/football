<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Runstring */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Бегущие строки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="runstring-view">


    <p>
        <?= Html::a('<i class="fa fa-pencil"></i> Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-circle']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'text:ntext',
            'start_date',
            'end_date',
        ],
    ]) ?>

</div>
