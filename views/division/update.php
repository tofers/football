<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Division */

$this->title = 'Редактирование дивизиона';
$this->params['breadcrumbs'][] = ['label' => 'Дивизионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="division-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
