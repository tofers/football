<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Division */
/* @var $form yii\widgets\ActiveForm */




$config = unserialize($model->config);
$matches = unserialize($model->matches);
print_r($matches);
if ($model->isNewRecord) {
    $model->published = 1;
    $config['score_win'] = 3;
    $config['score_draw'] = 1;
    $config['score_win_pos'] = 3;
    $config['score_win_tech'] = 3;
    $config['tech_goals'] = 5;
}

$type = isset($config['type']) ? $config['type'] : 0;
$score_win = isset($config['score_win']) ? $config['score_win'] : 0;
$score_draw = isset($config['score_draw']) ? $config['score_draw'] : 0;
$score_lose = isset($config['score_lose']) ? $config['score_lose'] : 0;
$score_win_pos = isset($config['score_win_pos']) ? $config['score_win_pos'] : 0;
$score_lose_pos = isset($config['score_lose_pos']) ? $config['score_lose_pos'] : 0;
$score_win_tech = isset($config['score_win_tech']) ? $config['score_win_tech'] : 0;
$score_lose_tech = isset($config['score_lose_tech']) ? $config['score_lose_tech'] : 0;
$score_both_lose_tech = isset($config['score_both_lose_tech']) ? $config['score_both_lose_tech'] : 0;
$tech_goals = isset($config['tech_goals']) ? $config['tech_goals'] : 0;
$description = isset($config['description']) ? $config['description'] : '';


$t1 = isset($matches['t1']) ? $matches['t1'] : '';
$t2 = isset($matches['t2']) ? $matches['t2'] : '';
$t3 = isset($matches['t3']) ? $matches['t3'] : '';
$t4 = isset($matches['t4']) ? $matches['t4'] : '';
$t5 = isset($matches['t5']) ? $matches['t5'] : '';
$t6 = isset($matches['t6']) ? $matches['t6'] : '';
$t7 = isset($matches['t7']) ? $matches['t7'] : '';
$t8 = isset($matches['t8']) ? $matches['t8'] : '';


$match_1_date = isset($matches['match_1']['date']) ? $matches['match_1']['date'] : '';
$match_2_date = isset($matches['match_2']['date']) ? $matches['match_2']['date'] : '';
$match_3_date = isset($matches['match_3']['date']) ? $matches['match_3']['date'] : '';
$match_4_date = isset($matches['match_4']['date']) ? $matches['match_4']['date'] : '';
$match_5_date = isset($matches['match_5']['date']) ? $matches['match_5']['date'] : '';
$match_6_date = isset($matches['match_6']['date']) ? $matches['match_6']['date'] : '';
$match_7_date = isset($matches['match_7']['date']) ? $matches['match_7']['date'] : '';
$match_8_date = isset($matches['match_8']['date']) ? $matches['match_8']['date'] : '';
$match_9_date = isset($matches['match_9']['date']) ? $matches['match_9']['date'] : '';
$match_10_date = isset($matches['match_10']['date']) ? $matches['match_10']['date'] : '';
$match_11_date = isset($matches['match_11']['date']) ? $matches['match_11']['date'] : '';
$match_12_date = isset($matches['match_12']['date']) ? $matches['match_12']['date'] : '';

$match_1_place = isset($matches['match_1']['place']) ? $matches['match_1']['place'] : '';
$match_2_place = isset($matches['match_2']['place']) ? $matches['match_2']['place'] : '';
$match_3_place = isset($matches['match_3']['place']) ? $matches['match_3']['place'] : '';
$match_4_place = isset($matches['match_4']['place']) ? $matches['match_4']['place'] : '';
$match_5_place = isset($matches['match_5']['place']) ? $matches['match_5']['place'] : '';
$match_6_place = isset($matches['match_6']['place']) ? $matches['match_6']['place'] : '';
$match_7_place = isset($matches['match_7']['place']) ? $matches['match_7']['place'] : '';
$match_8_place = isset($matches['match_8']['place']) ? $matches['match_8']['place'] : '';
$match_9_place = isset($matches['match_8']['place']) ? $matches['match_9']['place'] : '';
$match_10_place = isset($matches['match_10']['place']) ? $matches['match_10']['place'] : '';
$match_11_place = isset($matches['match_11']['place']) ? $matches['match_11']['place'] : '';
$match_12_place = isset($matches['match_12']['place']) ? $matches['match_12']['place'] : '';


$match_1_t1 = isset($matches['match_1']['t1']) ? $matches['match_1']['t1'] : '';
$match_2_t1 = isset($matches['match_2']['t1']) ? $matches['match_2']['t1'] : '';
$match_3_t1 = isset($matches['match_3']['t1']) ? $matches['match_3']['t1'] : '';
$match_4_t1 = isset($matches['match_4']['t1']) ? $matches['match_4']['t1'] : '';
$match_5_t1 = isset($matches['match_5']['t1']) ? $matches['match_5']['t1'] : '';
$match_6_t1 = isset($matches['match_6']['t1']) ? $matches['match_6']['t1'] : '';
$match_7_t1 = isset($matches['match_7']['t1']) ? $matches['match_7']['t1'] : '';
$match_8_t1 = isset($matches['match_8']['t1']) ? $matches['match_8']['t1'] : '';
$match_9_t1 = isset($matches['match_9']['t1']) ? $matches['match_9']['t1'] : '';
$match_10_t1 = isset($matches['match_10']['t1']) ? $matches['match_10']['t1'] : '';
$match_11_t1 = isset($matches['match_11']['t1']) ? $matches['match_11']['t1'] : '';
$match_12_t1 = isset($matches['match_12']['t1']) ? $matches['match_12']['t1'] : '';

$match_1_t2 = isset($matches['match_1']['t2']) ? $matches['match_1']['t2'] : '';
$match_2_t2 = isset($matches['match_2']['t2']) ? $matches['match_2']['t2'] : '';
$match_3_t2 = isset($matches['match_3']['t2']) ? $matches['match_3']['t2'] : '';
$match_4_t2 = isset($matches['match_4']['t2']) ? $matches['match_4']['t2'] : '';
$match_5_t2 = isset($matches['match_5']['t2']) ? $matches['match_5']['t2'] : '';
$match_6_t2 = isset($matches['match_6']['t2']) ? $matches['match_6']['t2'] : '';
$match_7_t2 = isset($matches['match_7']['t2']) ? $matches['match_7']['t2'] : '';
$match_8_t2 = isset($matches['match_8']['t2']) ? $matches['match_8']['t2'] : '';
$match_9_t2 = isset($matches['match_9']['t2']) ? $matches['match_9']['t2'] : '';
$match_10_t2 = isset($matches['match_10']['t2']) ? $matches['match_10']['t2'] : '';
$match_11_t2 = isset($matches['match_11']['t2']) ? $matches['match_11']['t2'] : '';
$match_12_t2 = isset($matches['match_12']['t2']) ? $matches['match_12']['t2'] : '';


?>

    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-map font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="division-form">
                <?php $form = ActiveForm::begin([
                ]); ?>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 ">
                        <?= $form->field($model, 'published')->checkbox([
                            'data-on-text' => 'ВКЛ',
                            'data-off-text' => 'ВЫКЛ'
                        ])->label('') ?>
                        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                        <div class="row">
                            <div class="col-lg-6">
                                <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($model, 'type')->dropDownList(\app\models\Division::$typeName, [
                                    'class' => 'selectpicker form-control'
                                ]) ?>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-lg-12">
                                <h5><b>Параметры подсчета очков матча</b></h5>
                                <div class="form-group">
                                    <label class="control-label">Тип статистики</label>
                                    <?= Html::dropDownList('Config[type]', $type, ['Футбол', 'Пляжный футбол'], ['class' => 'form-control selectpicker']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <br> <h5><b>Основное время</b></h5><br>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Очков за победу в основное <br>время</label>
                                    <?= Html::textInput('Config[score_win]', $score_win, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Очков за ничью в основное <br>время</label>
                                    <?= Html::textInput('Config[score_draw]', $score_draw, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label class="control-label">Очков за поражение в основное <br>время</label>
                                    <?= Html::textInput('Config[score_lose]', $score_lose, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <br><h5><b>Пенальти </b></h5><br>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Очков за победу по пенальти</label>
                                    <?= Html::textInput('Config[score_win_pos]', $score_win_pos, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Очков за поражение по пенальти</label>
                                    <?= Html::textInput('Config[score_lose_pos]', $score_lose_pos, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>


                            <div class="col-lg-12">
                                <br> <h5><b>Техническое поражение</b></h5><br>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="control-label">Очков за техническую победу</label>
                                    <?= Html::textInput('Config[score_win_tech]', $score_win_tech, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="control-label">Очков за техническое поражение</label>
                                    <?= Html::textInput('Config[score_lose_tech]', $score_lose_tech, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="control-label">Очков за обоюдное техническое
                                        поражение</label>
                                    <?= Html::textInput('Config[score_both_lose_tech]', $score_both_lose_tech, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label class="control-label">Голов за техническое поражение</label>
                                    <?= Html::textInput('Config[tech_goals]', $tech_goals, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label class="control-label">Описание</label>
                                    <?= Html::textarea('Config[description]', $description, ['class' => 'form-control', 'rows' => 6]) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>

<!--                                                              Чемпионат-->

                        <div class="row" id="division-type-content-2">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[t1]', $t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[t2]', $t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 3</label>
                                    <?= Html::textInput('Matches[t3]', $t3, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 4</label>
                                    <?= Html::textInput('Matches[t4]', $t4, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 5</label>
                                    <?= Html::textInput('Matches[t5]', $t5, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 6</label>
                                    <?= Html::textInput('Matches[t6]', $t6, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 7</label>
                                    <?= Html::textInput('Matches[t7]', $t7, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 8</label>
                                    <?= Html::textInput('Matches[t8]', $t8, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group hidden">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>


                                                        <!--Кубок-->

                <div class="row" id="division-type-content-1">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label"><b>1/4</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_1][date]', $match_1_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label"><b>1/4</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_2][date]', $match_2_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label"><b>1/4</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_3][date]', $match_3_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label"><b>1/4</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_4][date]', $match_4_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Площадка</label>
                            <?= Html::textInput('Matches[match_1][place]', $match_1_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Площадка:</label>
                            <?= Html::textInput('Matches[match_2][place]', $match_2_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Площадка</label>
                            <?= Html::textInput('Matches[match_3][place]', $match_3_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Площадка</label>
                            <?= Html::textInput('Matches[match_4][place]', $match_4_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_1][t1]', $match_1_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_2][t1]', $match_2_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_3][t1]', $match_3_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_4][t1]', $match_4_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_1][t2]', $match_1_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_2][t2]', $match_2_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_3][t2]', $match_3_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_4][t2]', $match_4_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>


                <div class="row" id="division-type-content-1">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>1/2</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_5][date]', $match_5_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>1/2</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_6][date]', $match_6_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка</label>
                                    <?= Html::textInput('Matches[match_5][place]', $match_5_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка:</label>
                                    <?= Html::textInput('Matches[match_6][place]', $match_6_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_5][t1]', $match_5_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_6][t1]', $match_6_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_5][t2]', $match_5_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_6][t2]', $match_6_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row" id="division-type-content-1">
                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label class="control-label"><b>Финал</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_7][date]', $match_7_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Площадка:</label>
                            <?= Html::textInput('Matches[match_7][place]', $match_7_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_7][t1]', $match_7_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_7][t2]', $match_7_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row" id="division-type-content-1">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>Отборочный за 5 место</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_8][date]', $match_8_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>Отборочный за 5 место</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_9][date]', $match_9_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка</label>
                                    <?= Html::textInput('Matches[match_8][place]', $match_8_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка:</label>
                                    <?= Html::textInput('Matches[match_9][place]', $match_9_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_8][t1]', $match_8_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_9][t1]', $match_9_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_8][t2]', $match_8_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_9][t2]', $match_9_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row" id="division-type-content-1">
                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label class="control-label"><b>5 место</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_10][date]', $match_10_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Площадка:</label>
                            <?= Html::textInput('Matches[match_10][place]', $match_10_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_10][t1]', $match_10_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_10][t2]', $match_10_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>

                <br><br>
                <div class="row" id="division-type-content-1">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>3 место</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_11][date]', $match_11_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>7 место</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_12][date]', $match_12_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка</label>
                                    <?= Html::textInput('Matches[match_11][place]', $match_11_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка:</label>
                                    <?= Html::textInput('Matches[match_12][place]', $match_12_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_11][t1]', $match_11_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_12][t1]', $match_12_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_11][t2]', $match_11_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_12][t2]', $match_12_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

<!--                кубок с выездными полуфиналами-->

                <div class="row" id="division-type-content-3">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>1/2 - 1</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_1][date]', $match_1_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>1/2 - 1</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_2][date]', $match_2_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка</label>
                                    <?= Html::textInput('Matches[match_1][place]', $match_1_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка:</label>
                                    <?= Html::textInput('Matches[match_2][place]', $match_2_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_1][t1]', $match_1_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_2][t1]', $match_2_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_1][t2]', $match_1_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_2][t2]', $match_2_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row" id="division-type-content-3">
                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label class="control-label"><b>Финал</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_5][date]', $match_5_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Площадка:</label>
                            <?= Html::textInput('Matches[match_5][place]', $match_5_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_5][t1]', $match_5_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_5][t2]', $match_5_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>

<br><br>
                <div class="row" id="division-type-content-3">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="row">

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>1/2 - 2</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_3][date]', $match_3_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label"><b>1/2 - 2</b><br> Дата и время</label>
                                    <?= Html::textInput('Matches[match_4][date]', $match_4_date, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка</label>
                                    <?= Html::textInput('Matches[match_3][place]', $match_3_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Площадка:</label>
                                    <?= Html::textInput('Matches[match_4][place]', $match_4_place, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_3][t1]', $match_3_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 1</label>
                                    <?= Html::textInput('Matches[match_4][t1]', $match_4_t1, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_3][t2]', $match_3_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Участник 2</label>
                                    <?= Html::textInput('Matches[match_4][t2]', $match_4_t2, ['class' => 'form-control']) ?>
                                    <div class="help-block"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="row" id="division-type-content-3">
                    <div class="col-lg-4 col-lg-offset-4">
                        <div class="form-group">
                            <label class="control-label"><b>3 место</b><br> Дата и время</label>
                            <?= Html::textInput('Matches[match_6][date]', $match_5_date, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Площадка:</label>
                            <?= Html::textInput('Matches[match_6][place]', $match_6_place, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 1</label>
                            <?= Html::textInput('Matches[match_6][t1]', $match_6_t1, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Участник 2</label>
                            <?= Html::textInput('Matches[match_6][t2]', $match_6_t2, ['class' => 'form-control']) ?>
                            <div class="help-block"></div>
                        </div>
                    </div>
                </div>


                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>
<?php
$this->registerJsFile('/js/division.js', [
    'depends' => ['app\assets\AppAsset'],
    'position' => yii\web\View::POS_END
]);
?>