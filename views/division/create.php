<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Division */

$this->title = 'Создать дивизион';
$this->params['breadcrumbs'][] = ['label' => 'Дивизионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="division-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
