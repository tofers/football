<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Division */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Дивизионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="division-view">


    <p>
        <?= Html::a('<i class="fa fa-pencil"></i> Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-circle']) ?>
        <?= Html::a(' <i class="fa fa-trash"></i> Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'season_id',
            'published',
            'type_name',
            'css:ntext',
            'title',
            'url:url',
            'sort',
            'matches:ntext',
            'config:ntext',
        ],
    ]) ?>

</div>
