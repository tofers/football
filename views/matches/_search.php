<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MatchSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $teams app\models\Team[] */
/* @var $divisions app\models\Division[] */
/* @var $referees app\models\Referee[] */

$referees = ArrayHelper::map($referees, 'id', 'full_name');
$divisions = ArrayHelper::map($divisions, 'id', 'title');
$teams = ArrayHelper::map($teams, 'id', 'name');
$area = [
    1 => 'Поле №1',
    2 => 'Поле №2',
    3 => 'Поле №3',
    4 => 'Поле №4',
    5 => 'Поле №5',
    6 => 'Поле №6',
    7 => 'Поле №7',
    8 => 'Поле №8',
];
$tour = [];
for ($i = 1; $i < 26; $i++) {
    $tour[$i] = 'Тур ' . $i;
}
?>

<div class="match-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-md-6 col-lg-4">
            <?= $form->field($model, 'period', [
                'template' => '{label} <div class="input-group reportrange"> {input} <span class="input-group-addon"><span class="fa fa-calendar"></span></span> </div> {error}'
            ])->textInput([
                'autocomplete' => 'off'
            ]) ?>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <?= $form->field($model, 'division_id')->dropDownList($divisions, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'data-size' => '10',
                'prompt' => 'Все'
            ]) ?>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <?php echo $form->field($model, 'referee_id')->dropDownList($referees, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'data-size' => '10',
                'prompt' => 'Все судьи'
            ]) ?>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-2">
            <?= $form->field($model, 'area')->dropDownList($area, [
                'class' => 'form-control selectpicker',
                'prompt' => 'Все'
            ]) ?>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <?php echo $form->field($model, 'team1_id')->dropDownList($teams, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'data-size' => '10',
                'prompt' => 'Все команды'
            ]) ?>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-3">
            <?php echo $form->field($model, 'team2_id')->dropDownList($teams, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'data-size' => '10',
                'prompt' => 'Все команды'
            ]) ?>
        </div>

        <div class="col-sm-6 col-md-4 col-lg-2">
            <?= $form->field($model, 'tour')->dropDownList($tour, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'prompt' => 'Все'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="">&nbsp;</label>
                <div>
                    <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary btn-circle']) ?>
                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
