<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Match */
/* @var $teams app\models\Team[] */
/* @var $divisions app\models\Division[] */
/* @var $referees app\models\Referee[] */
/* @var $videos app\models\Video[] */

$this->title = 'Редактирование матча';
$this->params['breadcrumbs'][] = ['label' => 'Матчи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="match-update">


    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
        'divisions' => $divisions,
        'referees' => $referees,
        'videos' => $videos,
    ]) ?>

</div>
