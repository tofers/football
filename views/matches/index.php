<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $teams app\models\Team */
/* @var $divisions app\models\Division */
/* @var $referees app\models\Referee */
/* @var $searchModel app\models\MatchSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список матчей';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-soccer-ball-o font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-plus"> </i> Добавить матч', ['create'], ['class' => 'btn btn-success btn-circle']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <?= $this->render('_search', [
                    'model' => $searchModel,
                    'teams' => $teams,
                    'divisions' => $divisions,
                    'referees' => $referees,
                ]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'columns' => [
                        [
                            'attribute' => 'date',
                            'format' => ['date', 'php:d.m.Y H:i'],
                            'contentOptions' => ['style' => 'width:200px;']
                        ],
                        [
                            'attribute' => 'division_id',
                            'value' => function ($model) {
                                return $model->division->title;
                            }
                        ],
                        [
                            'attribute' => 'tour',
                            'value' => function ($model) {
                                return $model->tourname;
                            }
                        ],
                        [
                            'attribute' => 'team1_id',
                            'value' => function ($model) {
                                return $model->team1->name;
                            }
                        ],
                        [
                            'attribute' => 'team2_id',
                            'value' => function ($model) {
                                return $model->team2->name;
                            }
                        ],
                        [
                            'attribute' => 'area',
                            'value' => function ($model) {
                                return $model->areaname;
                            }
                        ],
                        'score',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}{delete}',
                            'buttons' => [
                                'view' => function ($url, $model) {
                                    return Html::a('<i class="icon-eye"></i>', $url, ['class' => 'btn btn-outline btn-circle green btn-sm purple']);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fa fa-trash"></i>', $url, [
                                        'class' => 'btn btn-circle red btn-outline deleted',
                                        'data' => [
                                            'confirm' => 'Действительно хотите удалить?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                },
                            ],
                        ],
                    ],
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover'
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJsFile('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js', [
    'depends' => ['app\assets\AppAsset'],
    'position' => yii\web\View::POS_END
]);
$this->registerJsFile('/js/match.js', [
    'depends' => ['app\assets\AppAsset'],
    'position' => yii\web\View::POS_END
]);
$this->registerCssFile('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css', [
    'depends' => ['app\assets\AppAsset']
]);
?>
