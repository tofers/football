<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $players app\models\Player[] */
/* @var $players_enabled array[] */
/* @var $team_id int */

$players_enabled = ArrayHelper::map($players_enabled, 'player_id', 'player_id');
//print_r($players_enabled);
?>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th>ФИО</th>
        <th class="text-center">Участие</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($players as $player): ?>
        <tr>
            <td><?= $player->full_name ?></td>
            <td class="text-center">
                <?= Html::checkbox('Players['.$team_id.'][]', in_array($player->id, $players_enabled), [
                    'data-on-text' => 'ДА',
                    'data-off-text' => 'НЕТ',
                    'class' => 'switch',
                    'data-size' => "small",
                    'value' => $player->id
                ]) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
