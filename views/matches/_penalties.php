<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $players app\models\Player[] */
/* @var $team_id int */

$players = ArrayHelper::map($players, 'id', 'full_name');

$type = [
    'нереализован',
    'реализован',
];
$penalties = unserialize($penalties);
$penalties = $penalties?$penalties:[];
?>

<div class="mt-repeater">
    <div data-repeater-list="Penalties<?= $team_id ?>">
        <? foreach ($penalties as $penalti): ?>
            <div data-repeater-item="">
                <div class="mt-repeater-input">
                    <?= Html::dropDownList('player_id', $penalti['player_id'], $players, [
                        'class' => 'input-group form-control form-control-inline selectpicker-repeat',
                        'prompt' => 'Выберите игрока',
                        'data-width' => 'auto'
                    ]) ?>
                </div>
                <div class="mt-repeater-input">
                    <?= Html::dropDownList('result', $penalti['result'], $type, [
                        'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                    ]) ?>
                </div>
                <div class="mt-repeater-input">
                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger  ">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
        <div data-repeater-item="">
            <div class="mt-repeater-input">
                <?= Html::dropDownList('player_id', '', $players, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat',
                    'prompt' => 'Выберите игрока',
                    'data-width' => 'auto'
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::dropDownList('result', '', $type, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <a href="javascript:;" data-repeater-delete="" class="btn btn-danger  ">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
    </div>
    <a href="javascript:;" data-repeater-create="" class="btn btn-circle btn-sm btn-info mt-repeater-add">
        <i class="fa fa-plus"></i> Добавить </a>
</div>
