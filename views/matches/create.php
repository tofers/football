<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Match */
/* @var $teams app\models\Team[] */
/* @var $divisions app\models\Division[] */
/* @var $referees app\models\Referee[] */
/* @var $videos app\models\Video[] */

$this->title = 'Создание матча';
$this->params['breadcrumbs'][] = ['label' => 'Список матчей', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="match-create">

    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
        'divisions' => $divisions,
        'referees' => $referees,
        'videos' => $videos,
    ]) ?>

</div>
