<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Match */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Статистика матча. ' . $model->nameandtouranddiv;
$this->params['breadcrumbs'][] = ['label' => 'Матчи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$referees = ArrayHelper::map($referees, 'id', 'full_name');

$tech = [
    'Нет',
    $model->team1_name,
    $model->team2_name,
    'Обоюдное техническое поражение',
];
?>
    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-soccer-ball-o font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="match-view">

                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $form->field($model, 'was')->checkbox([
                                    'data-on-text' => 'Да',
                                    'data-off-text' => 'Нет'
                                ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $form->field($model, 'tech')->radioList($tech) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <?= $form->field($model, 'text')->textarea(['rows' => 6, 'class' => 'form-control summernote']) ?>
                            </div>
                        </div>
                        <h3 class="text-center"><b>Статистика</b></h3>

                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="text-center"><b><?= $model->team1_name ?></b></h4>
                                <?= $this->render('_players', [
                                    'players' => $model->team1->playerssostav,
                                    'team_id' => $model->team1_id,
                                    'players_enabled' => $model->players1
                                ]) ?>
                            </div>
                            <div class="col-md-6">
                                <h4 class="text-center"><b><?= $model->team2_name ?></b></h4>
                                <?= $this->render('_players', [
                                    'players' => $model->team2->playerssostav,
                                    'team_id' => $model->team2_id,
                                    'players_enabled' => $model->players2
                                ]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_goals1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_goals2')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-4 text-center">
                                Голы (первый тайм)
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_goals1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_goals2')->textInput()->label(false) ?>
                            </div>


                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_shots1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_shots2')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-4 text-center">
                                Удары по воротам (первый тайм)
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_shots1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_shots2')->textInput()->label(false) ?>
                            </div>


                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_gate1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_gate2')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-4 text-center">
                                Удары в створ ворот (первый тайм)
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_gate1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_gate2')->textInput()->label(false) ?>
                            </div>


                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_corner1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_corner2')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-4 text-center">
                                Угловые удары (первый тайм)
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_corner1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_corner2')->textInput()->label(false) ?>
                            </div>


                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_rail1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_rail2')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-4 text-center">
                                Штанга/перекладина (первый тайм)
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_rail1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_rail2')->textInput()->label(false) ?>
                            </div>


                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_fouls1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team1_fouls2')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-4 text-center">
                                Фолы (первый тайм - втрой тайм)
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_fouls1')->textInput()->label(false) ?>
                            </div>
                            <div class="col-lg-2">
                                <?= $form->field($model, 'team2_fouls2')->textInput()->label(false) ?>
                            </div>

                            <div class="col-lg-6">
                                <?= $form->field($model, 'referee_id')->dropDownList($referees, [
                                    'class' => 'form-control selectpicker',
                                    'prompt' => 'Выберите судью',
                                    'data-size' => '10',
                                    'data-live-search' => 'true',
                                ]) ?>
                            </div>
                            <div class="col-lg-6">
                                <?= $form->field($model, 'referee2_id')->dropDownList($referees, [
                                    'class' => 'form-control selectpicker',
                                    'prompt' => 'Выберите второго судью',
                                    'data-size' => '10',
                                    'data-live-search' => 'true',
                                ]) ?>
                            </div>


                            <div class="col-lg-3">
                                <?= $form->field($model, 'team1_referee')->dropDownList(['-', 1, 2, 3, 4, 5], ['selectpicker form-control'])->label(false) ?>
                            </div>
                            <div class="col-lg-6 text-center">
                                <div style="padding-top: 7px">Оценка судейства</div>
                            </div>
                            <div class="col-lg-3">
                                <?= $form->field($model, 'team2_referee')->dropDownList(['-', 1, 2, 3, 4, 5], ['selectpicker form-control'])->label(false) ?>
                            </div>

                            <div class="col-lg-3">
                                <?= $form->field($model, 'team1_discipline')->dropDownList(['-', 1, 2, 3, 4, 5], ['selectpicker form-control'])->label(false) ?>
                            </div>
                            <div class="col-lg-6 text-center">
                                <div style="padding-top: 7px">Дисциплина команд</div>
                            </div>
                            <div class="col-lg-3">
                                <?= $form->field($model, 'team2_discipline')->dropDownList(['-', 1, 2, 3, 4, 5], ['selectpicker form-control'])->label(false) ?>
                            </div>

                        </div>

                    </div>
                </div>
                <h4 class="text-center">Голы </h4>
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->render('_score', [
                            'players' => $model->team1->playerssostav,
                            'team_id' => $model->team1_id,
                            'goals' => $model->team1_goals_array
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->render('_score', [
                            'players' => $model->team2->playerssostav,
                            'team_id' => $model->team2_id,
                            'goals' => $model->team2_goals_array
                        ]) ?>
                    </div>
                </div>


                <h4 class="text-center">Карточки </h4>
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->render('_card', [
                            'players' => $model->team1->playerssostav,
                            'team_id' => $model->team1_id,
                            'cards' => $model->team1_cards_array
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->render('_card', [
                            'players' => $model->team2->playerssostav,
                            'team_id' => $model->team2_id,
                            'cards' => $model->team2_cards_array
                        ]) ?>
                    </div>
                </div>

                <h4 class="text-center"> Послематчевые пенальти </h4>
                <div class="row">
                    <div class="col-md-6">
                        <?= $this->render('_penalties', [
                            'players' => $model->team1->playerssostav,
                            'team_id' => $model->team1_id,
                            'penalties' => $model->team1_penalties_array
                        ]) ?>
                        <?//= $form->field($model, 'team1_penalties')->textInput() ?>
                    </div>
                    <div class="col-md-6">
                        <?= $this->render('_penalties', [
                            'players' => $model->team2->playerssostav,
                            'team_id' => $model->team2_id,
                            'penalties' => $model->team2_penalties_array
                        ]) ?>
                        <?//= $form->field($model, 'team2_penalties')->textInput() ?>
                    </div>
                </div>

                <div class="form-group hidden">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>


                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <div class="portlet-title portlet-footer">
            <div class="caption">
                <i class="fa fa-soccer-ball-o font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
    </div>

<?php
$this->registerJsFile('/js/match_view.js', [
    'depends' => ['app\assets\AppAsset'],
    'position' => yii\web\View::POS_END
]);