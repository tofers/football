<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $players app\models\Player[] */
/* @var $team_id int */

$players = ArrayHelper::map($players, 'id', 'full_name');

$type = [
    1 => 'желтая',
    2 => 'красная',
    3 => 'вторая желтая',
];

$cards = unserialize($cards);
$cards = $cards?$cards:[];
?>

<div class="mt-repeater">
    <div data-repeater-list="Card<?= $team_id ?>">

        <? foreach ($cards as $card): ?>
        <div data-repeater-item="">
            <div class="mt-repeater-input">
                <?= Html::dropDownList('player_id', $card['player_id'], $players, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                    'prompt' => 'Выберите игрока',
                    'data-width' => 'auto'
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::dropDownList('type', $card['type'], $type, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::textInput('min', $card['min'], ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Мин.']) ?>
            </div>
            <div class="mt-repeater-input">
                <a href="javascript:;" data-repeater-delete="" class="btn btn-danger ">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
        <? endforeach; ?>
        <div data-repeater-item="">
            <div class="mt-repeater-input">
                <?= Html::dropDownList('player_id', '', $players, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                    'prompt' => 'Выберите игрока',
                    'data-width' => 'auto'
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::dropDownList('type', '', $type, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::textInput('min', '', ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Мин.']) ?>
            </div>
            <div class="mt-repeater-input">
                <a href="javascript:;" data-repeater-delete="" class="btn btn-danger ">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
    </div>
    <a href="javascript:;" data-repeater-create="" class="btn btn-circle btn-sm btn-info mt-repeater-add">
        <i class="fa fa-plus"></i> Добавить </a>
</div>
