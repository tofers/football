<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $players app\models\Player[] */
/* @var $team_id int */

$players = ArrayHelper::map($players, 'id', 'full_name');

$hows = [
    1 => 'ногой',
    2 => 'головой',
    3 => '6-метровый',
    4 => 'автогол',
    5 => '10-метровый',
    10 => 'незабитый  пенальти',
    11 => 'незабитый  10-метровый пенальти',
];

$goals = unserialize($goals);
$goals = $goals ? $goals : [];
?>

<div class="mt-repeater">
    <div data-repeater-list="Score<?= $team_id ?>">
        <? foreach ($goals as $goal): ?>
            <div data-repeater-item="">
                <div class="mt-repeater-input">
                    <?= Html::dropDownList('player_id', $goal['player_id'], $players, [
                        'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                        'prompt' => 'Выберите игрока',
                        'data-width' => 'auto'
                    ]) ?>
                </div>
                <div class="mt-repeater-input">
                    <?= Html::dropDownList('how', $goal['how'], $hows, [
                        'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                        'data-width' => '100px'
                    ]) ?>
                </div>
                <div class="clearfix"></div>
                <div class="mt-repeater-input" style="padding-left: 0;">
                    <?= Html::dropDownList('assistent_id', $goal['assistent_id'], $players, [
                        'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                        'prompt' => 'Выберите ассисента',
                        'data-width' => 'auto'
                    ]) ?>
                </div>
                <div class="mt-repeater-input text-right">
                    <?= Html::textInput('min', $goal['min'], ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Мин.', 'style' => 'width: 75px;']) ?>
                </div>
                <div class="mt-repeater-input">
                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger ">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
                <hr>
            </div>
        <? endforeach; ?>
        <div data-repeater-item="">
            <div class="mt-repeater-input">
                <?= Html::dropDownList('player_id', '', $players, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                    'prompt' => 'Выберите игрока',
                    'data-width' => 'auto'
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::dropDownList('how', '', $hows, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                    'data-width' => '100px'
                ]) ?>
            </div>
            <div class="clearfix"></div>
            <div class="mt-repeater-input" style="padding-left: 0;">
                <?= Html::dropDownList('assistent_id', '', $players, [
                    'class' => 'input-group form-control form-control-inline selectpicker-repeat ',
                    'prompt' => 'Выберите ассисента',
                    'data-width' => 'auto'
                ]) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::textInput('min', '', ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Мин.']) ?>
            </div>
            <div class="mt-repeater-input">
                <a href="javascript:;" data-repeater-delete="" class="btn btn-danger ">
                    <i class="fa fa-close"></i>
                </a>
            </div>
            <hr style="margin: 10px 0;">
        </div>
    </div>
    <a href="javascript:;" data-repeater-create="" class="btn btn-circle btn-sm btn-info mt-repeater-add">
        <i class="fa fa-plus"></i> Добавить </a>
</div>
