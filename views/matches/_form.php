<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Match */
/* @var $form yii\widgets\ActiveForm */
/* @var $teams app\models\Team[] */
/* @var $divisions app\models\Division[] */
/* @var $referees app\models\Referee[] */

$referees = ArrayHelper::map($referees, 'id', 'full_name');
$divisions = ArrayHelper::map($divisions, 'id', 'title');
$teams = ArrayHelper::map($teams, 'id', 'name');
$videos = ArrayHelper::map($teams, 'id', 'title');
$area = [
    1 => 'Поле №1',
    2 => 'Поле №2',
    3 => 'Поле №3',
    4 => 'Поле №4',
    5 => 'Поле №5',
    6 => 'Поле №6',
    7 => 'Поле №7',
    8 => 'Поле №8',
];
$tour = [];
for ($i = 1; $i < 26; $i++) {
    $tour[$i] = 'Тур ' . $i;
}

if ($model->isNewRecord) {
    $model->date = date('d.m.Y H:i');
}


$model->date = ($model->date) ? strtotime($model->date) : '';
if ($model->date) {
    $model->date = date('d.m.Y H:i', $model->date);
}

//echo '<pre>';
//print_r($model->errors);
//echo '</pre>';
?>
<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-soccer-ball-o font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                    <? if (!$model->isNewRecord): ?>
                        <?= Html::a('<i class="icon-eye "></i> Просмотр', ['matches/view/' . $model->id], ['class' => 'btn btn-circle purple']) ?>
                    <? endif; ?>
                    <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="match-form">

                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
                            <div class="row">
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'date', [
                                        'template' => '{label} <div class="input-group datetime"> {input} <span class="input-group-addon"><span class="fa fa-calendar"></span></span> </div> {error}'
                                    ]) ?>
                                </div>
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'division_id')->dropDownList($divisions, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите дивизион'
                                    ]) ?>
                                </div>
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'tour')->dropDownList($tour, [
                                        'class' => 'form-control selectpicker',
                                        'prompt' => 'Выберите тур'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'team1_id')->dropDownList($teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите хозяев'
                                    ]) ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'team2_id')->dropDownList($teams, [
                                        'class' => 'form-control selectpicker',
                                        'data-live-search' => 'true',
                                        'data-size' => '10',
                                        'prompt' => 'Выберите гостей'
                                    ]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'stadium')->textInput() ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'area')->dropDownList($area, [
                                        'class' => 'form-control selectpicker',
                                        'prompt' => 'Выберите поле'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'referee_id')->dropDownList($referees, [
                                        'class' => 'form-control selectpicker',
                                        'prompt' => 'Выберите судью',
                                        'data-size' => '10',
                                        'data-live-search' => 'true',
                                    ]) ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'referee2_id')->dropDownList($referees, [
                                        'class' => 'form-control selectpicker',
                                        'prompt' => 'Выберите второго судью',
                                        'data-size' => '10',
                                        'data-live-search' => 'true',
                                    ]) ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'video_id')->dropDownList($videos, [
                                'class' => 'form-control selectpicker',
                                'prompt' => 'Выберите видео'
                            ]) ?>
                        </div>
                    </div>


                    <? //= $form->field($model, 'team1_name')->textInput(['maxlength' => true]) ?>





                    <? //= $form->field($model, 'referee_id')->textInput() ?>

                    <? //= $form->field($model, 'referee2_id')->textInput() ?>

                    <? //= $form->field($model, 'tech')->textInput() ?>

                    <? //= $form->field($model, 'was')->textInput() ?>

                    <? //= $form->field($model, 'text')->textarea(['rows' => 6]) ?>



                    <? //= $form->field($model, 'team1_score')->textInput() ?>

                    <? //= $form->field($model, 'team1_penalties')->textInput() ?>

                    <? //= $form->field($model, 'team1_penalties_array')->textarea(['rows' => 6]) ?>

                    <? //= $form->field($model, 'team1_goals_array')->textarea(['rows' => 6]) ?>

                    <? //= $form->field($model, 'team1_goals1')->textInput() ?>

                    <? //= $form->field($model, 'team1_goals2')->textInput() ?>

                    <? //= $form->field($model, 'team1_shots1')->textInput() ?>

                    <? //= $form->field($model, 'team1_shots2')->textInput() ?>

                    <? //= $form->field($model, 'team1_gate1')->textInput() ?>

                    <? //= $form->field($model, 'team1_gate2')->textInput() ?>

                    <? //= $form->field($model, 'team1_corner1')->textInput() ?>

                    <? //= $form->field($model, 'team1_corner2')->textInput() ?>

                    <? //= $form->field($model, 'team1_rail1')->textInput() ?>

                    <? //= $form->field($model, 'team1_rail2')->textInput() ?>

                    <? //= $form->field($model, 'team1_fouls1')->textInput() ?>

                    <? //= $form->field($model, 'team1_fouls2')->textInput() ?>

                    <? //= $form->field($model, 'team1_cards_array')->textarea(['rows' => 6]) ?>

                    <? //= $form->field($model, 'team1_referee')->textInput() ?>

                    <? //= $form->field($model, 'team1_discipline')->textInput() ?>


                    <? //= $form->field($model, 'team2_name')->textInput(['maxlength' => true]) ?>

                    <? //= $form->field($model, 'team2_score')->textInput() ?>

                    <? //= $form->field($model, 'team2_penalties')->textInput() ?>

                    <? //= $form->field($model, 'team2_penalties_array')->textarea(['rows' => 6]) ?>

                    <? //= $form->field($model, 'team2_goals_array')->textarea(['rows' => 6]) ?>

                    <? //= $form->field($model, 'team2_goals1')->textInput() ?>

                    <? //= $form->field($model, 'team2_goals2')->textInput() ?>

                    <? //= $form->field($model, 'team2_shots1')->textInput() ?>

                    <? //= $form->field($model, 'team2_shots2')->textInput() ?>

                    <? //= $form->field($model, 'team2_gate1')->textInput() ?>

                    <? //= $form->field($model, 'team2_gate2')->textInput() ?>

                    <? //= $form->field($model, 'team2_corner1')->textInput() ?>

                    <? //= $form->field($model, 'team2_corner2')->textInput() ?>

                    <? //= $form->field($model, 'team2_rail1')->textInput() ?>

                    <? //= $form->field($model, 'team2_rail2')->textInput() ?>

                    <? //= $form->field($model, 'team2_fouls1')->textInput() ?>

                    <? //= $form->field($model, 'team2_fouls2')->textInput() ?>

                    <? //= $form->field($model, 'team2_cards_array')->textarea(['rows' => 6]) ?>

                    <? //= $form->field($model, 'team2_referee')->textInput() ?>

                    <? //= $form->field($model, 'team2_discipline')->textInput() ?>

                    <? //= $form->field($model, 'sort')->textInput() ?>


                    <div class="form-group hidden">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>


