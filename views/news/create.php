<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = 'Создание новости';
$this->params['breadcrumbs'][] = ['label' => 'Пресса', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-create">
    <?= $this->render('_form', [
        'model' => $model,
        'matches' => $matches,
    ]) ?>
</div>
