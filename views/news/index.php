<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пресса';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-newspaper-o font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-plus"> </i> Добавить новость', ['create'], ['class' => 'btn btn-success btn-circle']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <?= $this->render('_search', ['model' => $searchModel]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                   // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        [
                            'attribute' => 'date',
                            'format' => ['date', 'php:d.m.Y'],
                        ],
                        'title',
                       // 'img',
                        //'published',
                        'status:html',
                        //'description:ntext',
                        //'text:ntext',
                        //'matches:ntext',
                        //'link',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                            'buttons' => [
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fa fa-trash"></i>', $url, [
                                        'class' => 'btn btn-circle red btn-outline deleted',
                                        'data' => [
                                            'confirm' => 'Действительно хотите удалить?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                },
                            ],
                        ],
                    ],
                    'tableOptions'=>[
                        'class'=> 'table table-striped table-hover'
                    ]
                ]); ?>
            </div>
        </div>
    </div>
</div>