<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $disqualifications string */
/* @var $players app\models\Player[] */

$disqualifications = unserialize($disqualifications);
?>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th rowspan="2">ФИО</th>
        <th colspan="7"  class="text-center">Номер тура</th>
        <th colspan="3"  class="text-center">Кубок</th>
    </tr>
    <tr>
        <th class="text-center">1</th>
        <th class="text-center">2</th>
        <th class="text-center">3</th>
        <th class="text-center">4</th>
        <th class="text-center">5</th>
        <th class="text-center">6</th>
        <th class="text-center">7</th>
        <th class="text-center">1</th>
        <th class="text-center">2</th>
        <th class="text-center">3</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($players as $player): ?>
        <tr>
            <td><?= $player->full_name ?></td>
            <td>
                <?= Html::checkbox('Disq[' . $player->id . '][0][1]', isset($disqualifications[$player->id][0][1]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][0][2]', isset($disqualifications[$player->id][0][2]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][0][3]', isset($disqualifications[$player->id][0][3]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][0][4]', isset($disqualifications[$player->id][0][4]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][0][5]', isset($disqualifications[$player->id][0][5]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][0][6]', isset($disqualifications[$player->id][0][6]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][0][7]', isset($disqualifications[$player->id][0][7]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][1][1]', isset($disqualifications[$player->id][1][1]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][1][2]', isset($disqualifications[$player->id][1][2]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
            <td>
                <?= Html::checkbox('Disc[' . $player->id . '][1][3]', isset($disqualifications[$player->id][1][3]), [
                    'data-on-text' => '<i class="fa fa-check"></i>',
                    'data-off-text' => '<i class="fa fa-times"></i>',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => 1
                ]) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>