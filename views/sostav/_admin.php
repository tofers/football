<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */


$administrations = unserialize($administrations);
$administrations = $administrations ? $administrations : [];
?>

<div class="mt-repeater">
    <div data-repeater-list="Administrations">

        <? foreach ($administrations as $administration): ?>
            <?
            if (!$administration['name']) continue;
            ?>
            <div data-repeater-item="">
                <div class="mt-repeater-input">
                    <?= Html::textInput('photo', $administration['photo'], ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Фотография']) ?>
                </div>
                <div class="mt-repeater-input">
                    <?= Html::textInput('name', $administration['name'], ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Иия']) ?>
                </div>
                <div class="mt-repeater-input">
                    <?= Html::textInput('work', $administration['work'], ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Должность']) ?>
                </div>
                <div class="mt-repeater-input">
                    <a href="javascript:;" data-repeater-delete="" class="btn btn-danger ">
                        <i class="fa fa-close"></i>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
        <div data-repeater-item="">
            <div class="mt-repeater-input">
                <?= Html::textInput('photo', '', ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Фотография']) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::textInput('name', '', ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Иия']) ?>
            </div>
            <div class="mt-repeater-input">
                <?= Html::textInput('work', '', ['class' => 'input-group form-control form-control-inline ', 'placeholder' => 'Должность']) ?>
            </div>
            <div class="mt-repeater-input">
                <a href="javascript:;" data-repeater-delete="" class="btn btn-danger ">
                    <i class="fa fa-close"></i>
                </a>
            </div>
        </div>
    </div>
    <a href="javascript:;" data-repeater-create="" class="btn btn-circle btn-sm btn-info mt-repeater-add">
        <i class="fa fa-plus"></i> Добавить </a>
</div>
