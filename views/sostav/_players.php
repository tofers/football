<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $players app\models\Player[] */
/* @var $players_enabled array[] */
/* @var $revoked_enabled array[] */

?>

<table class="table table-striped table-hover" id="teamsostamplayers">
    <thead>
    <tr>
        <th>ФИО</th>
        <th class="text-center">
            Заявка<br>
            <button class="btn btn-primary btn-xs" id="checkOn">Да</button>
            <button class="btn btn-default btn-xs" id="checkOff">Нет</button>
        </th>
        <th class="text-center">
            Отозван<br>
            <button class="btn btn-primary btn-xs" id="checkOn">Да</button>
            <button class="btn btn-default btn-xs" id="checkOff">Нет</button>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($players as $player): ?>
        <tr>
            <td><?= $player->full_name ?></td>
            <td class="text-center">
                <?= Html::checkbox('Players[]', in_array($player->id, $players_enabled), [
                    'data-on-text' => 'ДА',
                    'data-off-text' => 'НЕТ',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => $player->id
                ]) ?>
            </td>
            <td class="text-center">
                <?= Html::checkbox('Revoked[]', in_array($player->id, $revoked_enabled), [
                    'data-on-text' => 'ДА',
                    'data-off-text' => 'НЕТ',
                    'class' => 'switch',
                    'data-size' => "mini",
                    'value' => $player->id
                ]) ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
