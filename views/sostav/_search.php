<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\SostavSearch */
/* @var $form yii\widgets\ActiveForm */

$teams = ArrayHelper::map($teams, 'id', 'name');
?>

<div class="team-sostav-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">

        <div class="col-sm-6 col-md-4 col-lg-3">
            <?php echo $form->field($model, 'team_id')->dropDownList($teams, [
                'class' => 'form-control selectpicker',
                'data-live-search' => 'true',
                'data-size' => '10',
                'prompt' => 'Все команды'
            ]) ?>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="">&nbsp;</label>
                <div>
                    <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary btn-circle']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
