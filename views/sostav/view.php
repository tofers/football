<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TeamSostav */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Составы команд', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-sostav-view">


    <p>
        <?= Html::a(' <i class="fa fa-pencil"></i> Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-circle']) ?>
        <?= Html::a('<i class="fa fa-trash"></i>  Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'season_id',
            'team.name',
            'players:ntext',
            'revoked:ntext',
            'administration:ntext',
            'disc:ntext',
        ],
    ]) ?>

</div>
