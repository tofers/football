<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TeamSostav */

$this->title = 'Редактирование состава';
$this->params['breadcrumbs'][] = ['label' => 'Составы команд', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->team->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="team-sostav-update">


    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
    ]) ?>

</div>
