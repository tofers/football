<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\TeamSostav */
/* @var $form yii\widgets\ActiveForm */

$teams = ArrayHelper::map($teams, 'id', 'name');

$players_enabled = explode(',', $model->players);
$revoked_enabled = explode(',', $model->revoked);

//print_r($model->errors);
?>


    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-users font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
        <div class="portlet-body">
            <div class="team-sostav-form">

                <?php $form = ActiveForm::begin(); ?>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 ">

                        <div class="row">
                            <? if ($model->isNewRecord): ?>
                                <div class="col-lg-4 col-lg-offset-4">
                                    <?= $form->field($model, 'team_id')->dropDownList($teams, [
                                        'class' => 'selectpicker form-control',
                                        'data-live-search' => 'true',
                                        'prompt' => 'Выберите команду'
                                    ]) ?>
                                </div>
                            <? endif; ?>
                            <div class="col-xs-12">
                                <div id="players-list">
                                    <? if (!$model->isNewRecord || $model->team_id): ?>
                                        <?= $this->render('_players', [
                                            'players' => $model->team->players,
                                            'players_enabled' => $players_enabled,
                                            'revoked_enabled' => $revoked_enabled,
                                        ]) ?>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="admins-list">
                            <h4 class="text-center"><b>Админстрация</b></h4>
                            <?= $this->render('_admin', [
                                'administrations' => $model->administration
                            ]) ?>
                        </div>
                        <div class="disq-list">
                            <h4 class="text-center"><b>Дисквалификации</b></h4>
                            <div class="font-sm help-block">
                                Этот список только для организационных дисквалификаций. Не используйте его для
                                дисквалификаций по карточкам.
                                <br>
                                Так как список туров строится на основе дивизионов, они должны быть определены.
                                <br>
                                <br>
                                Если игрока нет в списке, добавьте его в заявку выше и сохраните изменения.
                            </div>
                            <div id="disq-list">
                                <? if (!$model->isNewRecord || $model->team_id): ?>
                                    <?= $this->render('_disc', [
                                        'players' => $model->team->players,
                                        'disqualifications' => $model->disc
                                    ]) ?>
                                <? endif; ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="form-group hidden">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <div class="portlet-title portlet-footer">
            <div class="caption">
                <i class="fa fa-soccer-ball-o font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
    </div>

<?php
$this->registerJsFile('/js/sostav.js', [
    'depends' => ['app\assets\AppAsset'],
    'position' => yii\web\View::POS_END
]);
?>