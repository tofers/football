<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TeamSostav */

$this->title = 'Создать команду';
$this->params['breadcrumbs'][] = ['label' => 'Составы команд', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-sostav-create">


    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
    ]) ?>

</div>
