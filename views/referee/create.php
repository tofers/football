<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Referee */

$this->title = 'Создать судью';
$this->params['breadcrumbs'][] = ['label' => 'Судьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="referee-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
