<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Referee */

$this->title = 'Редактирование судьи';
$this->params['breadcrumbs'][] = ['label' => 'Судьи', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="referee-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
