<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MediaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="media-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-6 col-md-4 col-lg-3">
            <?= $form->field($model, 'title') ?>
        </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label for="">&nbsp;</label>
            <div>
                <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary btn-circle']) ?>
            </div>
        </div>
    </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
