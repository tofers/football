<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Media */

$this->title = 'Добавить статью';
$this->params['breadcrumbs'][] = ['label' => 'СМИ о нас', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
