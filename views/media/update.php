<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Media */

$this->title = 'Редактирование статьи';
$this->params['breadcrumbs'][] = ['label' => 'СМИ о нас', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="media-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
