<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Player */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Игроки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-view">


    <p>
        <?= Html::a('<i class="fa fa-pencil"></i> Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-circle']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'name',
            'bthdate',
            'weight',
            'growth',
            'work_place',
            [
                'attribute' => 'photo',
                'format' => ['image',['width'=>'100']],
            ],
            'team.name',
            'position',
        ],
    ]) ?>

</div>
