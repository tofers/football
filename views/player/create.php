<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Player */
/* @var $teams app\models\Team */

$this->title = 'Создать игрока';
$this->params['breadcrumbs'][] = ['label' => 'Игроки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-create">


    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
    ]) ?>

</div>
