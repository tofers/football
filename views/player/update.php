<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Player */
/* @var $teams app\models\Team[] */

$this->title = 'Редактирование игрока';
$this->params['breadcrumbs'][] = ['label' => 'Игроки', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="player-update">


    <?= $this->render('_form', [
        'model' => $model,
        'teams' => $teams,
    ]) ?>

</div>
