<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Player */
/* @var $teams app\models\Team[] */
/* @var $form yii\widgets\ActiveForm */


$teams = \yii\helpers\ArrayHelper::map($teams, 'id', 'name');

$model->birthdate = ($model->birthdate && $model->birthdate != '0000-00-00') ? strtotime($model->birthdate) : '';
if ($model->birthdate) {
    $model->birthdate = date('d.m.Y', $model->birthdate);
}

?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-user font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-angle-left"></i> Назад',Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                    <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="player-form">

                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data'
                        ]]); ?>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 ">
                            <div class="row">
                                <div class="col-lg-8">

                                    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'team_id')->dropDownList($teams, [
                                        'class' => 'selectpicker form-control',
                                        'data-live-search' => 'true'
                                    ]) ?>
                                </div>
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'work_place')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'position')->dropDownList(\app\models\Player::$namePosition, [
                                        'class' => 'selectpicker form-control'
                                    ]) ?>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'birthdate', [
                                        'template' => '{label} <div class="input-group date"> {input} <span class="input-group-addon"><span class="fa fa-calendar"></span></span> </div> {error}'
                                    ]) ?>
                                </div>
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'weight')->textInput(['class' => 'number_input form-control']) ?>
                                </div>
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'growth')->textInput(['class' => 'number_input form-control']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?=Html::activeLabel($model,'photo',['class'=>'control-label'])?>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <div class="fileinput fileinput-<?= $model->photo ? 'exists' : 'new' ?>"
                                         data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 325px; height: 425px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 325px; max-height: 425px;">
                                            <img id="img-logo" src="<?= $model->photo ?>" alt=""
                                                 style="max-height: 425px;"/>
                                        </div>
                                        <div>
                                    <span class="btn blue btn-circle btn-file">
                                        <span class="fileinput-new  "> Загрузить </span>
                                        <span class="fileinput-exists"> Изменить </span>
                                        <input type="file" class="blue  btn btn-circle" name="Player[photo]"
                                               accept="image/*">
                                    </span>
                                            <?= Html::a('Удалить', 'javascript:;', [
                                                'class' => 'btn red fileinput-exists btn-circle',
                                                'data-dismiss' => 'fileinput'
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group hidden">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>