<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$city = [
    'msk' => 'Москва',
    'spb' => 'Санкт-Петербург',
    'krn' => 'Красноярск',
    'vrn' => 'Воронеж',
    'sochi' => 'Сочи',
];

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h3 class="form-title">Войдите в свой аккаунт</h3>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form'
    ]); ?>

    <?= $form->field($model, 'username')->textInput([
        'autofocus' => true,
        'autocomplete' => 'off',
        'required' => 'required',
        'class' => 'form-control placeholder-no-fix',
        'placeholder' => 'Имя пользователя'
    ])->label(false) ?>

    <?= $form->field($model, 'password')->passwordInput([
        'autocomplete' => 'off',
        'required' => 'required',
        'class' => 'form-control placeholder-no-fix',
        'placeholder' => 'Пароль'
    ])->label(false) ?>

    <?= $form->field($model, 'city')->dropDownList($city, [
        'autocomplete' => 'off',
        'required' => 'required',
        'class' => 'form-control placeholder-no-fix selectpicker',
    ])->label(false) ?>


    <div class="form-group">
        <div class="text-right">
            <?= Html::submitButton('Авторизоваться', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>
