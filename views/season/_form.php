<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Season */
/* @var $form yii\widgets\ActiveForm */

if ($model->isNewRecord) {
    $model->published = 1;
    $model->start_date =  time();
}else $model->start_date = ($model->start_date && $model->start_date != '0000-00-00') ? strtotime($model->start_date) : '';
if ($model->start_date) {
    $model->start_date = date('d.m.Y', $model->start_date);
}

//print_r($model->errors);
?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-map font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                    <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="season-form">

                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 ">
                            <?/*= $form->field($model, 'published')->checkbox([
                                'data-on-text' => 'ВКЛ',
                                'data-off-text' => 'ВЫКЛ'
                            ])->label('')*/ ?>

                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                            <div class="row">
                                <div class="col-lg-4">
                                    <?= $form->field($model, 'start_date', [
                                        'template' => '{label} <div class="input-group date"> {input} <span class="input-group-addon"><span class="fa fa-calendar"></span></span> </div> {error}'
                                    ])?>
                                </div>

                                <div class="col-lg-8">
                                    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div></div>

                    </div>



                    <div class="form-group hidden">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>