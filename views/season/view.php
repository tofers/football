<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Season */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Seasons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="season-view">


    <p>
        <?= Html::a(' <i class="fa fa-pencil"></i> Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-circle' ]) ?>
        <?= Html::a('<i class="fa fa-trash"></i>  Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Действительно хотите удалить?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'city_id',
            'url:url',
            'title',
            [
                'attribute' => 'start_date',
                'format' => ['date', 'php:d.m.Y']
            ],
            'published',
        ],
    ]) ?>

</div>
