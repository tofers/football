<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Season */

$this->title = 'Создать сезон';
$this->params['breadcrumbs'][] = ['label' => 'Сезоны', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="season-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
