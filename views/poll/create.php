<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Poll */

$this->title = 'Создание голосования';
$this->params['breadcrumbs'][] = ['label' => 'Голосования', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poll-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
