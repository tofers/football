<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $form yii\widgets\ActiveForm */


$model->date = ($model->date) ? strtotime($model->date) : '';
if ($model->isNewRecord) {
    $model->date = time();
}
if ($model->date) {
    $model->date = date('d.m.Y', $model->date);
}

//print_r($model->errors);
?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-camera font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                    <? if (!$model->isNewRecord): ?>
                        <?= Html::a('<i class="icon-eye "></i> Просмотр', ['gallery/view/' . $model->id], ['class' => 'btn btn-circle purple']) ?>
                    <? endif; ?>
                    <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="gallery-form">

                    <?php $form = ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data'
                        ]
                    ]); ?>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                            <div class="row">
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'date', [
                                        'template' => '{label} <div class="input-group date"> {input} <span class="input-group-addon"><span class="fa fa-calendar"></span></span> </div> {error}'
                                    ]) ?>
                                </div>
                                <div class="col-lg-6">
                                    <?= $form->field($model, 'archive_url')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Html::activeLabel($model, 'logo', ['class' => 'control-label']) ?>
                                <div class="clearfix"></div>
                                <div class="text-center">
                                    <div class="fileinput fileinput-<?= $model->logo ? 'exists' : 'new' ?>"
                                         data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 192px; height: 192px;"></div>
                                        <div class="fileinput-preview fileinput-exists thumbnail"
                                             style="max-width: 192px; max-height: 192px;">
                                            <img id="img-logo" src="<?= $model->logo ?>" alt=""
                                                 style="max-height: 192px;"/>
                                        </div>
                                        <div>
                                    <span class="btn blue btn-circle btn-file">
                                        <span class="fileinput-new  "> Загрузить </span>
                                        <span class="fileinput-exists"> Изменить </span>
                                        <input type="file" class="blue  btn btn-circle" name="Gallery[logo]"
                                               accept="image/*">
                                    </span>
                                            <?= Html::a('Удалить', 'javascript:;', [
                                                'class' => 'btn red fileinput-exists btn-circle',
                                                'data-dismiss' => 'fileinput'
                                            ]) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="form-group hidden">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>