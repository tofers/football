<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Галереи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$modelPhoto = new \app\models\GalleryPhoto();
?>


    <div class="portlet light portlet-fit portlet-datatable bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-camera font-green"></i>
                <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
            </div>
            <div class="actions">
                <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
            </div>
        </div>
        <div class="portlet-body">

            <?= Html::button('<i class="fa fa-check"></i> Добавить фотографии', ['class' => 'btn btn-success btn-circle', 'data-toggle' => "modal", 'data-target' => "#modalPhotos"]) ?>

            <div id="gallery-list">
                <?= $this->render('_photo', [
                    'photos' => $model->photos,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="modalPhotos">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Загрузка фотографий</h4>
                </div>
                <div class="modal-body">
                    <a href="javascript:; " id="downloadPhoto">
                        <i class="icon-plus"></i> Прикрепить файл

                    </a>
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'options' => [
                            'enctype' => 'multipart/form-data',
                            'style' => 'display:none;',
                            'id' => 'formPhotos',
                            'data-key' => $model->id
                        ]
                    ]); ?>
                    <?= $form->field($modelPhoto, 'filename[]')->fileInput(['multiple' => 'multiple', 'accept' => 'image/*']) ?>


                    <?php \yii\widgets\ActiveForm::end(); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-circle" data-dismiss="modal">Закрыть</button>
                    <button type="button" class="btn btn-primary btn-circle" id="savePhoto"
                            data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Сохранить">
                        Сохранить
                    </button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<?php
$this->registerJsFile('/js/photos.js?v=1', [
    'depends' => ['app\assets\AppAsset'],
    'position' => yii\web\View::POS_END
]);
