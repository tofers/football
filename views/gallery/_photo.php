<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery */
/* @var $form yii\widgets\ActiveForm */
/* @var $photos \app\models\GalleryPhoto[] */

$link = '/msk/football/referee/media-share-0-02-04-2da39c2e3f2ad18903e358f161570787dcd0b9673ec3cfdd27f8cc0a5b38e3d1-76e7af19-4a6e-462b-9204-58a38c270fe9.jpg';
?>
<br>
<div class="row gallery">
    <? foreach ($photos as $photo): ?>
        <? $link = $photo->filename ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 gallery-item">
            <div class="thumbnail">
                <a href="<?= $link ?>">
                    <img src="<?= $link ?>" alt="">
                </a>
                <div class="caption text-center">
                    <ul class="post-links">
                        <li>
                            <a href="#">
                                <i class=" icon-link"></i>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <?= Html::checkbox('', false, ['value' => $photo->id]) ?>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <i class=" icon-trash red"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>
