<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Баннеры';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="portlet light portlet-fit portlet-datatable bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-picture font-green"></i>
            <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
        </div>
        <div class="actions">
            <?= Html::a('<i class="fa fa-plus"> </i> Добавить баннер', ['create'], ['class' => 'btn btn-success btn-circle']) ?>
        </div>
    </div>
    <div class="portlet-body">
        <?= $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'title',
                'link',
                //'filename',
                //'html_code:ntext',
                //'width',
                //'height',
                //'published',
                [
                    'attribute' => 'published',
                    'format'=>'html',
                    'value' => function ($model) {
                        return $model->status;
                    }
                ],
                // 'type',
                [
                    'attribute' => 'type',
                    'value' => function ($model) {
                        return $model->type_name;
                    }
                ],
                'shows',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url, $model, $key) {
                            return Html::a('<i class="fa fa-trash"></i>', $url, [
                                'class' => 'btn btn-circle red btn-outline deleted',
                                'data' => [
                                    'confirm' => 'Действительно хотите удалить?',
                                    'method' => 'post',
                                ],
                            ]);
                        },
                    ],
                ],
            ],
            'tableOptions' => [
                'class' => 'table table-striped table-hover'
            ]
        ]); ?>
    </div>
</div>