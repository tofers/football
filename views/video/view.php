<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Видео', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-view">


    <p>
        <?= Html::a('<i class="fa fa-pencil"></i>  Редактирование', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-circle']) ?>
        <?= Html::a('<i class="fa fa-trash"></i>  Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-circle',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'date',
            'img',
            'code:ntext',
        ],
    ]) ?>

</div>
