<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-camcorder font-green"></i>
                    <span class="caption-subject font-green sbold uppercase"> <?= Html::encode($this->title) ?> </span>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="fa fa-angle-left"></i> Назад', Yii::$app->request->referrer, ['class' => 'btn btn-default btn-circle']) ?>
                    <?= Html::button('<i class="fa fa-check"></i> Сохранить', ['class' => 'btn btn-success btn-circle', 'id' => 'saveForm']) ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="video-form">

                    <?php $form = ActiveForm::begin(); ?>

                    <div class="row">
                        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 ">
                            <div class="row">
                                <div class=" col-lg-4">

                                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class=" col-lg-4">
                                    <?= $form->field($model, 'date', [
                                        'template' => '{label} <div class="input-group date"> {input} <span class="input-group-addon"><span class="fa fa-calendar"></span></span> </div> {error}'
                                    ]) ?>
                                </div>
                                <div class=" col-lg-4">

                                    <?= $form->field($model, 'img')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>
                            <?= $form->field($model, 'code')->textarea(['rows' => 6]) ?>
                        </div>
                    </div>



                    <div class="form-group hidden">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>